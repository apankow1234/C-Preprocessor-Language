import re

class Node:
    
    ALL_Nodes = []
    
    def __init__(self,name):
        self.name = Node.ValidateName(name)
        Node.ALL_Nodes.append(self)
        
    @staticmethod
    def Find(name):
        """Find Node in ALL_Nodes list."""
        for node in Node.ALL_Nodes:
            if node.name == name:
                return node
        return None
    
    @staticmethod
    def ValidateName(name, sep):
        """Make sure all Nodes have a unique name by adding an incremental suffix if the name already exists."""
        matches = []
        suffixes = []
        get_suffix = re.compile(name+"(\d+)")
        for node in Node.ALL_Nodes:
            if name in node.name:
                matches.append(node.name)
        if not suffixes:
            return name
        for m in matches:
            if re.match(get_suffix, m, re.I):
                suffixes.append(int(re.match(get_suffix, m, re.I)))
        return name + sep + str(max(suffixes)+1)
    
class DAG(Node):
    
    ALL_Nodes = []
    Roots = []
    
    def __init__(self, name):
        Node.__init__(self, name)
        DAG.ALL_Nodes.append(self)
        DAG.Roots.append(self)
        self.parents = []
        self.children = []
        
        