/*
	Andrew Pankow
	Language c

	C's Preprocessor Language
*/

#ifndef CNLPPL_H
#define CNLPPL_H
/*
	C's Preprocessor Language

	My goal was to recreate the <Template>T ability from c++ in c and to have the ability
	to create order-n vectors, matrices, and tensors. I was specifically going for the
	ability to dynamically work with 2, 3, 4, 7, and 8-dimensional objects.

	Graphics uses :
	* vectors (2D,3D,and 4D)
	* matrices (2D,3D,and 4D)
	* quaternions (complex 4D) //complex meaning using lateral/imaginary numbers

	Other uses :
	* tensors
	* 7D cross-products
	* octonions

	It has become clear, however, that I have built an extension to a macro system that
	acts or will act like an interpreted language unto itself. I am now going to be building
	it up to include common formulae that exist in spreadsheet applications so that
	new-comers to this extension will have familiarity with it from get.
*/


/*
	Numbers will be a good place to start...

	For the main header, I take it up to base-16. But I've included up to base-99 in the
	extended header. It is my recommendation that you stick to base-10 for most things.
	Everything else is more-or-less for fun at this stage. One reason to include a higher
	base than normal, might be to extend your looping abilities (hexadecimal, perhaps) per
	place-value.
*/
#define USE_BASE 10
#define USE_NUM_PLACE_VALUES 2


/* Should you desire, for whatever reason, I've included base-2 up to base-99. */
#if USE_BASE > 1
	#include "cppl_number_bases.h"
#endif /* USE_BASE */


#if defined(USE_NUM_PLACE_VALUES)
	/*Contains up to three place-values*/
	#include "cppl_place_values.h"
#endif /* USE_NUM_PLACE_VALUES */


/*
	The Foundations...
	shout out to pfultz2: https://github.com/pfultz2/Cloak

	I have made adjustments, changed some names, and added comments that made sense to
	me. Overall, I have deviated significantly from this initial starting point but I
	can't forget my roots. Peace!
*/
#define CONCAT(x,...) x##__VA_ARGS__ /*concatenate (literally) the first argument to the rest.
 	 	 	 	 	 	 	 	 	 If there are more than 2 arguments,
 	 	 	 	 	 	 	 	 	 the second onward will remain comma separated*/

#define CLEAN(x,...) CONCAT(x,__VA_ARGS__) /*Make sure any macros passed as arguments are rendered
											before concatenating*/

#define __iif(boolean) CONCAT(__iif_,boolean) /*Not, if-and-only-if, just a proto/basic IF()*/
#define __iif_1(trueValue,falseValue) trueValue /*same statement, first answer*/
#define __iif_0(trueValue,falseValue) falseValue /*same statement, other answer*/

#define __compliment(boolean) CONCAT(__compliment_,boolean) /*If 1, return 0. If 0, return 1.*/
#define __compliment_0 1 /*Case where zero, one is returned.*/
#define __compliment_1 0 /*Case where one, zero is returned.*/

#define __check_n(x,n,...) n /*based on __check and __probe, should return 1 or 0*/
#define __check(...) __check_n(__VA_ARGS__,0,) /*pop 0 onto the end, just in case*/
#define __probe(x) x, 1, /*pop 1 onto the end because x is at least something*/

#define IS_PAREN(x,...) __check(__is_paren_probe x) /*looking for valid empty token*/
#define __is_paren_probe(...) __probe(~) /*add a token to make sure not just null*/

#define BOOL(boolean) __compliment(NOT(boolean)) /*confirms the boolean*/
#define NOT(boolean) __check(CONCAT(__not_,boolean)) /*cleanses the boolean*/
#define __not_0 __probe('anything') /*tests against the boolean*/

#define IF(boolean) __iif(BOOL(boolean))/*send (true, false) commands inline*/
#define END(...) /*Arguments are left to Limbo and returns blank/nothing*/
#define RUN(...) __VA_ARGS__ /*Break-out functionality to allow continuing.*/
#define WHEN(boolean) IF(boolean)(RUN,END) /*not a loop but an IF() without a false*/

#define IS_DEFINED(definition,x) IS_PAREN(CLEAN(definition##x)(())) /**/
#define BOTH_DEFINED(x,y) IS_PAREN(COMPARE_##x(COMPARE_##y)(()))
#define IS_DEFINED_NUMERAL(x) IS_DEFINED(COMPARE_,x)

#define NOT_EQUAL(x,y) \
	IF(AND(IS_DEFINED_NUMERAL(x),IS_DEFINED_NUMERAL(y))) /*if both are defined*/\
	( \
	   BOTH_DEFINED, /*Will call the ending params of (x,y)*/ \
	   1 END /*Will call the ending params of (x,y), so use END function to terminate*/ \
	)(x,y)
#define EQUAL(x,y) __compliment(NOT_EQUAL(x,y))

#define X_LESS_THAN_OR_EQUAL_TO(x,max) CLEAN(CONCAT(IS_,max),_OR_LESS)(x) /*0 <= x <= 100*/
#define X_LESS_THAN(x,max) AND(NOT_EQUAL(x,max),X_LESS_THAN_OR_EQUAL_TO(x,max)) /*0 <= x < 100*/
#define X_GREATER_THAN_OR_EQUAL_TO(x,min) OR(EQUAL(x,min),NOT(X_LESS_THAN_OR_EQUAL_TO(x,min))) /*0 <= x*/
#define X_GREATER_THAN(x,min) NOT(X_LESS_THAN_OR_EQUAL_TO(x,min)) /*0 < x*/
#define X_BETWEEN(x,min,max) /*min and max order doesn't matter*/ \
	OR( \
		AND(X_GREATER_THAN(x,min),X_LESS_THAN(x,max)) \
		,AND(X_LESS_THAN(x,min),X_GREATER_THAN(x,max)) \
	)
#define X_INBETWEEN(x,min,max) /*min and max order doesn't matter*/ \
	OR( \
		AND(X_GREATER_THAN_OR_EQUAL_TO(x,min),X_LESS_THAN_OR_EQUAL_TO(x,max)) \
		AND(X_LESS_THAN_OR_EQUAL_TO(x,min),X_GREATER_THAN_OR_EQUAL_TO(x,max)) \
	)

/*
	Binary Compare...
*/
#define OR(x,y) CLEAN(__or_,CLEAN(x,y))
#define __or_00 0
#define __or_01 1
#define __or_10 1
#define __or_11 1

#define NOR(x,y) NOT(OR(x,y))

#define AND(x,y) CLEAN(__and_,CLEAN(x,y))
#define __and_00 0
#define __and_01 0
#define __and_10 0
#define __and_11 1

#define NAND(x,y) NOT(AND(x,y))

#define XOR(x,y) CLEAN(__xor_,CLEAN(x,y))
#define __xor_00 0
#define __xor_01 1
#define __xor_10 1
#define __xor_11 0


#define PREPEND_1(_1,...) _1,__VA_ARGS__
#define PREPEND_2(_1,_2,...) _1,_2,__VA_ARGS__
#define PREPEND_3(_1,_2,_3,...) _1,_2,_3,__VA_ARGS__
#define POP(x,...) x
#define POP_2(x,y,...) x,y
#define POP_3(x,y,z,...) x,y,z
#define POP_4(x,y,z,w,...) x,y,z,w
#define DUMP_FIRST_ARG(x,...) __VA_ARGS__
#define DUMP_FIRST_2_ARGS(x,y,...) __VA_ARGS__


/*
	Sadly non-dynamic methods for counting...
	shout out to:
		Laurent Deniau, https://groups.google.com/forum/#!topic/comp.std.c/d-6Mj5Lko_s
	for this concept.
	Here is my understanding of it:

	Use PROTO_COUNT(...) and it will push the inverse sequence onto your
	stack of arguments so that 99 is touching the last argument you typed.
	0 will be on the far end. Now we'll get the offset by putting all the
	arguments into their own pockets starting with your first one heading
	toward 0 but only for 100 pockets. Unless you're crazy, you didn't
	come close to 100 arguments passed so there will be a number in the
	100th pocket. Since we started the numbers in reverse, the number from
	zero to the 100th pocket is the count of arguments.

        Arguments (A#) ----> sliding the numbers off the back end.
        /-----------------------------------------------------------------------/↓↓
----> A4|24 23 22 21 20 19 18 17 16 15 14 13 12 11 10 09 08 07 06 05 04 03 02 01|00
        /-----------------------------------------------------------------------/↑↑
                                                                               count


        /-----------------------------------------------------------------------/↓↓
        |A1 A2 A3 A4 A5 24 23 22 21 20 19 18 17 16 15 14 13 12 11 10 09 08 07 06|05 04 03 02 01 00
        /-----------------------------------------------------------------------/↑↑
                                                                               count

        Numbers <---- sliding the numbers onto the back end.
        /-----------------------------------------------------------------------/↓↓
        |A1 A2 A3 A4 A5             <----       24 23 22 21 20 19 18 17 16 15 14|... <----
        /-----------------------------------------------------------------------/↑↑
                                                                               count


        /-----------------------------------------------------------------------/↓↓
        |A1 A2 A3 A4 A5 24 23 22 21 20 19 18 17 16 15 14 13 12 11 10 09 08 07 06|05 ...
        /-----------------------------------------------------------------------/↑↑
                                                                               count
*/
#define INVERSE_SEQUENCE() \
	99,98,97,96,95,94,93,92,91,90, \
	89,88,87,86,85,84,83,82,81,80, \
	79,78,77,76,75,74,73,72,71,70, \
	69,68,67,66,65,64,63,62,61,60, \
	59,58,57,56,55,54,53,52,51,50, \
	49,48,47,46,45,44,43,42,41,40, \
	39,38,37,36,35,34,33,32,31,30, \
	29,28,27,26,25,24,23,22,21,20, \
	19,18,17,16,15,14,13,12,11,10, \
	9, 8, 7, 6, 5, 4, 3, 2, 1, 0
#define ARGUMENT_OFFSET( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11,_12,_13,_14,_15,_16,_17,_18,_19,_20, \
	_21,_22,_23,_24,_25,_26,_27,_28,_29,_30, \
	_31,_32,_33,_34,_35,_36,_37,_38,_39,_40, \
	_41,_42,_43,_44,_45,_46,_47,_48,_49,_50, \
	_51,_52,_53,_54,_55,_56,_57,_58,_59,_60, \
	_61,_62,_63,_64,_65,_66,_67,_68,_69,_70, \
	_71,_72,_73,_74,_75,_76,_77,_78,_79,_80, \
	_81,_82,_83,_84,_85,_86,_87,_88,_89,_90, \
	_91,_92,_93,_94,_95,_96,_97,_98,_99, N, \
	...) N
#define POP_COUNT(...) ARGUMENT_OFFSET(__VA_ARGS__)
#define PROTO_COUNT(...) POP_COUNT(__VA_ARGS__,INVERSE_SEQUENCE())


/* Limit 0 to 100... had to "hand/hard" code the ability */
#define COUNT(...) \
	IF(EMPTY_SEQUENCE(__VA_ARGS__)) \
	( \
		0 \
		,PROTO_COUNT(__VA_ARGS__) \
	)

#define COUNT_ARGS(...) PROTO_COUNT(__VA_ARGS__)
#define NUM_ARGS_IS(x,...) EQUAL(x,COUNT_ARGS(__VA_ARGS__))

#define __blank_comp_(x) x
#define IS_BLANK(x,...) IS_DEFINED(__blank_comp_,x)
#define IS_FIRST_EMPTY(...) IF(AND(IS_BLANK(__VA_ARGS__),NOT(IS_PAREN(POP(__VA_ARGS__)))))(1,0)
#define EMPTY_SEQUENCE(...) AND(IS_FIRST_EMPTY(__VA_ARGS__),IS_FIRST_EMPTY(DUMP_FIRST_ARG(__VA_ARGS__)))


#define EMPTY(...) IS_BLANK(__VA_ARGS__) //NEEDS more...

#define IS_EMPTY(...) IS_BLANK(__VA_ARGS__) //NEEDS more...



#define ANY_ARGS(...) NOT(EMPTY( __VA_ARGS__ ))





#endif /* CNLPPL_H */
