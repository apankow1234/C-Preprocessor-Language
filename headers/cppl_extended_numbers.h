/*
	Andrew Pankow
	Language c
*/
#ifndef CNLPPL_EXTENDED_PLACES_H
#define CNLPPL_EXTENDED_PLACES_H
/*
	C's Preprocessor Language - Extended Place-Values


	This header extends the initial header into further number systems and can easily
	be appended to for continuation down the rabbit hole.

	No, I didn't do any of this by hand...
	I used a spreadsheet application; even for formatting.
*/

/* #define USE_HEPTADECIMAL_NUMBERS       // base-17 */
/* #define USE_OCTODECIMAL_NUMBERS        // base-18 */
/* #define USE_NONDECIMAL_NUMBERS         // base-19 */
/* #define USE_VIGESIMAL_NUMBERS          // base-20 */
/* #define USE_UNVIGESIMAL_NUMBERS        // base-21 */
/* #define USE_DUOVIGESIMAL_NUMBERS       // base-22 */
/* #define USE_TRIVIGESIMAL_NUMBERS       // base-23 */
/* #define USE_TETRAVIGESIMAL_NUMBERS     // base-24 */
/* #define USE_PENTAVIGESIMAL_NUMBERS     // base-25 */
/* #define USE_HEXAVIGESIMAL_NUMBERS      // base-26 */
/* #define USE_HEPTAVIGESIMAL_NUMBERS     // base-27 */
/* #define USE_OCTOVIGESIMAL_NUMBERS      // base-28 */
/* #define USE_NONVIGESIMAL_NUMBERS       // base-29 */
/* #define USE_TRIGESIMAL_NUMBERS         // base-30 */
/* #define USE_UNTRIGESIMAL_NUMBERS       // base-31 */
/* #define USE_DUOTRIGESIMAL_NUMBERS      // base-32 */
/* #define USE_TRITRIGESIMAL_NUMBERS      // base-33 */
/* #define USE_TETRATRIGESIMAL_NUMBERS    // base-34 */
/* #define USE_PENTATRIGESIMAL_NUMBERS    // base-35 */
/* #define USE_HEXATRIGESIMAL_NUMBERS     // base-36 */
/* #define USE_HEPTATRIGESIMAL_NUMBERS    // base-37 */
/* #define USE_OCTOTRIGESIMAL_NUMBERS     // base-38 */
/* #define USE_NONTRIGESIMAL_NUMBERS      // base-39 */
/* #define USE_QUADRAGESIMAL_NUMBERS      // base-40 */
/* #define USE_UNQUADRAGESIMAL_NUMBERS    // base-41 */
/* #define USE_DUOQUADRAGESIMAL_NUMBERS   // base-42 */
/* #define USE_TRIQUADRAGESIMAL_NUMBERS   // base-43 */
/* #define USE_TETRAQUADRAGESIMAL_NUMBERS // base-44 */
/* #define USE_PENTAQUADRAGESIMAL_NUMBERS // base-45 */
/* #define USE_HEXAQUADRAGESIMAL_NUMBERS  // base-46 */
/* #define USE_HEPTAQUADRAGESIMAL_NUMBERS // base-47 */
/* #define USE_OCTOQUADRAGESIMAL_NUMBERS  // base-48 */
/* #define USE_NONQUADRAGESIMAL_NUMBERS   // base-49 */
/* #define USE_QUINQUAGESIMAL_NUMBERS     // base-50 */
/* #define USE_UNQUINQUAGESIMAL_NUMBERS   // base-51 */
/* #define USE_DUOQUINQUAGESIMAL_NUMBERS  // base-52 */
/* #define USE_TRIQUINQUAGESIMAL_NUMBERS  // base-53 */
/* #define USE_TETRAQUINQUAGESIMAL_NUMBERS// base-54 */
/* #define USE_PENTAQUINQUAGESIMAL_NUMBERS// base-55 */
/* #define USE_HEXAQUINQUAGESIMAL_NUMBERS // base-56 */
/* #define USE_HEPTAQUINQUAGESIMAL_NUMBERS// base-57 */
/* #define USE_OCTOQUINQUAGESIMAL_NUMBERS // base-58 */
/* #define USE_NONQUINQUAGESIMAL_NUMBERS  // base-59 */
/* #define USE_SEXAGESIMAL_NUMBERS        // base-60 */
/* #define USE_UNSEXAGESIMAL_NUMBERS      // base-61 */
/* #define USE_DUOSEXAGESIMAL_NUMBERS     // base-62 */
/* #define USE_TRISEXAGESIMAL_NUMBERS     // base-63 */
/* #define USE_TETRASEXAGESIMAL_NUMBERS   // base-64 */
/* #define USE_PENTASEXAGESIMAL_NUMBERS   // base-65 */
/* #define USE_HEXASEXAGESIMAL_NUMBERS    // base-66 */
/* #define USE_HEPTASEXAGESIMAL_NUMBERS   // base-67 */
/* #define USE_OCTOSEXAGESIMAL_NUMBERS    // base-68 */
/* #define USE_NONSEXAGESIMAL_NUMBERS     // base-69 */
/* #define USE_SEPTAGESIMAL_NUMBERS       // base-70 */
/* #define USE_UNSEPTAGESIMAL_NUMBERS     // base-71 */
/* #define USE_DUOSEPTAGESIMAL_NUMBERS    // base-72 */
/* #define USE_TRISEPTAGESIMAL_NUMBERS    // base-73 */
/* #define USE_TETRASEPTAGESIMAL_NUMBERS  // base-74 */
/* #define USE_PENTASEPTAGESIMAL_NUMBERS  // base-75 */
/* #define USE_HEXASEPTAGESIMAL_NUMBERS   // base-76 */
/* #define USE_HEPTASEPTAGESIMAL_NUMBERS  // base-77 */
/* #define USE_OCTOSEPTAGESIMAL_NUMBERS   // base-78 */
/* #define USE_NONSEPTAGESIMAL_NUMBERS    // base-79 */
/* #define USE_OCTOGESIMAL_NUMBERS        // base-80 */
/* #define USE_UNOCTOGESIMAL_NUMBERS      // base-81 */
/* #define USE_DUOOCTOGESIMAL_NUMBERS     // base-82 */
/* #define USE_TRIOCTOGESIMAL_NUMBERS     // base-83 */
/* #define USE_TETRAOCTOGESIMAL_NUMBERS   // base-84 */
/* #define USE_PENTAOCTOGESIMAL_NUMBERS   // base-85 */
/* #define USE_HEXAOCTOGESIMAL_NUMBERS    // base-86 */
/* #define USE_HEPTAOCTOGESIMAL_NUMBERS   // base-87 */
/* #define USE_OCTOOCTOGESIMAL_NUMBERS    // base-88 */
/* #define USE_NONOCTOGESIMAL_NUMBERS     // base-89 */
/* #define USE_NONAGESIMAL_NUMBERS        // base-90 */
/* #define USE_UNNONAGESIMAL_NUMBERS      // base-91 */
/* #define USE_DUONONAGESIMAL_NUMBERS     // base-92 */
/* #define USE_TRINONAGESIMAL_NUMBERS     // base-93 */
/* #define USE_TETRANONAGESIMAL_NUMBERS   // base-94 */
/* #define USE_PENTANONAGESIMAL_NUMBERS   // base-95 */
/* #define USE_HEXANONAGESIMAL_NUMBERS    // base-96 */
/* #define USE_HEPTANONAGESIMAL_NUMBERS   // base-97 */
/* #define USE_OCTONONAGESIMAL_NUMBERS    // base-98 */
/* #define USE_NONNONAGESIMAL_NUMBERS     // base-99 */

#if USE_BASE == 99 || defined(USE_NONNONAGESIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_OCTONONAGESIMAL_NUMBERS
		#define USE_OCTONONAGESIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_98(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 98
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_97
		#define _inc_97 98
	#endif
	#ifndef _inc_98
		#define _inc_98 0
	#endif
	#ifndef _dec_0
		#define _dec_0 98
	#endif
	#define _dec_98 97

	/* logic */
	#define IS_98_OR_LESS(x) OR(IS_97_OR_LESS(x),EQUAL(98,x))

	/* looping */
	#define PROTO_FROM_98(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,98)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),98),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),98)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,98,do_macro(98,__VA_ARGS__))

#endif /* BASE-99 : USE_NONNONAGESIMAL_NUMBERS */


#if USE_BASE == 98 || defined(USE_OCTONONAGESIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_HEPTANONAGESIMAL_NUMBERS
		#define USE_HEPTANONAGESIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_97(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 97
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_96
		#define _inc_96 97
	#endif
	#ifndef _inc_97
		#define _inc_97 0
	#endif
	#ifndef _dec_0
		#define _dec_0 97
	#endif
	#define _dec_97 96

	/* logic */
	#define IS_97_OR_LESS(x) OR(IS_96_OR_LESS(x),EQUAL(97,x))

	/* looping */
	#define PROTO_FROM_97(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,97)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),97),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),97)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,97,do_macro(97,__VA_ARGS__))

#endif /* BASE-98 : USE_OCTONONAGESIMAL_NUMBERS */


#if USE_BASE == 97 || defined(USE_HEPTANONAGESIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_HEXANONAGESIMAL_NUMBERS
		#define USE_HEXANONAGESIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_96(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 96
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_95
		#define _inc_95 96
	#endif
	#ifndef _inc_96
		#define _inc_96 0
	#endif
	#ifndef _dec_0
		#define _dec_0 96
	#endif
	#define _dec_96 95

	/* logic */
	#define IS_96_OR_LESS(x) OR(IS_95_OR_LESS(x),EQUAL(96,x))

	/* looping */
	#define PROTO_FROM_96(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,96)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),96),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),96)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,96,do_macro(96,__VA_ARGS__))

#endif /* BASE-97 : USE_HEPTANONAGESIMAL_NUMBERS */


#if USE_BASE == 96 || defined(USE_HEXANONAGESIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_PENTANONAGESIMAL_NUMBERS
		#define USE_PENTANONAGESIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_95(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 95
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_94
		#define _inc_94 95
	#endif
	#ifndef _inc_95
		#define _inc_95 0
	#endif
	#ifndef _dec_0
		#define _dec_0 95
	#endif
	#define _dec_95 94

	/* logic */
	#define IS_95_OR_LESS(x) OR(IS_94_OR_LESS(x),EQUAL(95,x))

	/* looping */
	#define PROTO_FROM_95(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,95)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),95),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),95)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,95,do_macro(95,__VA_ARGS__))

#endif /* BASE-96 : USE_HEXANONAGESIMAL_NUMBERS */


#if USE_BASE == 95 || defined(USE_PENTANONAGESIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_TETRANONAGESIMAL_NUMBERS
		#define USE_TETRANONAGESIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_94(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 94
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_93
		#define _inc_93 94
	#endif
	#ifndef _inc_94
		#define _inc_94 0
	#endif
	#ifndef _dec_0
		#define _dec_0 94
	#endif
	#define _dec_94 93

	/* logic */
	#define IS_94_OR_LESS(x) OR(IS_93_OR_LESS(x),EQUAL(94,x))

	/* looping */
	#define PROTO_FROM_94(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,94)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),94),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),94)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,94,do_macro(94,__VA_ARGS__))

#endif /* BASE-95 : USE_PENTANONAGESIMAL_NUMBERS */


#if USE_BASE == 94 || defined(USE_TETRANONAGESIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_TRINONAGESIMAL_NUMBERS
		#define USE_TRINONAGESIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_93(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 93
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_92
		#define _inc_92 93
	#endif
	#ifndef _inc_93
		#define _inc_93 0
	#endif
	#ifndef _dec_0
		#define _dec_0 93
	#endif
	#define _dec_93 92

	/* logic */
	#define IS_93_OR_LESS(x) OR(IS_92_OR_LESS(x),EQUAL(93,x))

	/* looping */
	#define PROTO_FROM_93(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,93)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),93),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),93)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,93,do_macro(93,__VA_ARGS__))

#endif /* BASE-94 : USE_TETRANONAGESIMAL_NUMBERS */


#if USE_BASE == 93 || defined(USE_TRINONAGESIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_DUONONAGESIMAL_NUMBERS
		#define USE_DUONONAGESIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_92(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 92
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_91
		#define _inc_91 92
	#endif
	#ifndef _inc_92
		#define _inc_92 0
	#endif
	#ifndef _dec_0
		#define _dec_0 92
	#endif
	#define _dec_92 91

	/* logic */
	#define IS_92_OR_LESS(x) OR(IS_91_OR_LESS(x),EQUAL(92,x))

	/* looping */
	#define PROTO_FROM_92(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,92)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),92),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),92)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,92,do_macro(92,__VA_ARGS__))

#endif /* BASE-93 : USE_TRINONAGESIMAL_NUMBERS */


#if USE_BASE == 92 || defined(USE_DUONONAGESIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_UNNONAGESIMAL_NUMBERS
		#define USE_UNNONAGESIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_91(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 91
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_90
		#define _inc_90 91
	#endif
	#ifndef _inc_91
		#define _inc_91 0
	#endif
	#ifndef _dec_0
		#define _dec_0 91
	#endif
	#define _dec_91 90

	/* logic */
	#define IS_91_OR_LESS(x) OR(IS_90_OR_LESS(x),EQUAL(91,x))

	/* looping */
	#define PROTO_FROM_91(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,91)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),91),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),91)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,91,do_macro(91,__VA_ARGS__))

#endif /* BASE-92 : USE_DUONONAGESIMAL_NUMBERS */


#if USE_BASE == 91 || defined(USE_UNNONAGESIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_NONAGESIMAL_NUMBERS
		#define USE_NONAGESIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_90(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 90
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_89
		#define _inc_89 90
	#endif
	#ifndef _inc_90
		#define _inc_90 0
	#endif
	#ifndef _dec_0
		#define _dec_0 90
	#endif
	#define _dec_90 89

	/* logic */
	#define IS_90_OR_LESS(x) OR(IS_89_OR_LESS(x),EQUAL(90,x))

	/* looping */
	#define PROTO_FROM_90(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,90)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),90),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),90)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,90,do_macro(90,__VA_ARGS__))

#endif /* BASE-91 : USE_UNNONAGESIMAL_NUMBERS */


#if USE_BASE == 90 || defined(USE_NONAGESIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_NONOCTAGESIMAL_NUMBERS
		#define USE_NONOCTAGESIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_89(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 89
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_88
		#define _inc_88 89
	#endif
	#ifndef _inc_89
		#define _inc_89 0
	#endif
	#ifndef _dec_0
		#define _dec_0 89
	#endif
	#define _dec_89 88

	/* logic */
	#define IS_89_OR_LESS(x) OR(IS_88_OR_LESS(x),EQUAL(89,x))

	/* looping */
	#define PROTO_FROM_89(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,89)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),89),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),89)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,89,do_macro(89,__VA_ARGS__))

#endif /* BASE-90 : USE_NONAGESIMAL_NUMBERS */


#if USE_BASE == 89 || defined(USE_NONOCTAGESIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_OCTOOCTAGESIMAL_NUMBERS
		#define USE_OCTOOCTAGESIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_88(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 88
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_87
		#define _inc_87 88
	#endif
	#ifndef _inc_88
		#define _inc_88 0
	#endif
	#ifndef _dec_0
		#define _dec_0 88
	#endif
	#define _dec_88 87

	/* logic */
	#define IS_88_OR_LESS(x) OR(IS_87_OR_LESS(x),EQUAL(88,x))

	/* looping */
	#define PROTO_FROM_88(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,88)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),88),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),88)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,88,do_macro(88,__VA_ARGS__))

#endif /* BASE-89 : USE_NONOCTAGESIMAL_NUMBERS */


#if USE_BASE == 88 || defined(USE_OCTOOCTAGESIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_HEPTAOCTAGESIMAL_NUMBERS
		#define USE_HEPTAOCTAGESIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_87(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 87
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_86
		#define _inc_86 87
	#endif
	#ifndef _inc_87
		#define _inc_87 0
	#endif
	#ifndef _dec_0
		#define _dec_0 87
	#endif
	#define _dec_87 86

	/* logic */
	#define IS_87_OR_LESS(x) OR(IS_86_OR_LESS(x),EQUAL(87,x))

	/* looping */
	#define PROTO_FROM_87(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,87)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),87),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),87)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,87,do_macro(87,__VA_ARGS__))

#endif /* BASE-88 : USE_OCTOOCTAGESIMAL_NUMBERS */


#if USE_BASE == 87 || defined(USE_HEPTAOCTAGESIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_HEXAOCTAGESIMAL_NUMBERS
		#define USE_HEXAOCTAGESIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_86(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 86
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_85
		#define _inc_85 86
	#endif
	#ifndef _inc_86
		#define _inc_86 0
	#endif
	#ifndef _dec_0
		#define _dec_0 86
	#endif
	#define _dec_86 85

	/* logic */
	#define IS_86_OR_LESS(x) OR(IS_85_OR_LESS(x),EQUAL(86,x))

	/* looping */
	#define PROTO_FROM_86(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,86)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),86),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),86)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,86,do_macro(86,__VA_ARGS__))

#endif /* BASE-87 : USE_HEPTAOCTAGESIMAL_NUMBERS */


#if USE_BASE == 86 || defined(USE_HEXAOCTAGESIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_PENTAOCTAGESIMAL_NUMBERS
		#define USE_PENTAOCTAGESIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_85(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 85
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_84
		#define _inc_84 85
	#endif
	#ifndef _inc_85
		#define _inc_85 0
	#endif
	#ifndef _dec_0
		#define _dec_0 85
	#endif
	#define _dec_85 84

	/* logic */
	#define IS_85_OR_LESS(x) OR(IS_84_OR_LESS(x),EQUAL(85,x))

	/* looping */
	#define PROTO_FROM_85(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,85)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),85),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),85)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,85,do_macro(85,__VA_ARGS__))

#endif /* BASE-86 : USE_HEXAOCTAGESIMAL_NUMBERS */


#if USE_BASE == 85 || defined(USE_PENTAOCTAGESIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_TETRAOCTAGESIMAL_NUMBERS
		#define USE_TETRAOCTAGESIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_84(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 84
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_83
		#define _inc_83 84
	#endif
	#ifndef _inc_84
		#define _inc_84 0
	#endif
	#ifndef _dec_0
		#define _dec_0 84
	#endif
	#define _dec_84 83

	/* logic */
	#define IS_84_OR_LESS(x) OR(IS_83_OR_LESS(x),EQUAL(84,x))

	/* looping */
	#define PROTO_FROM_84(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,84)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),84),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),84)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,84,do_macro(84,__VA_ARGS__))

#endif /* BASE-85 : USE_PENTAOCTAGESIMAL_NUMBERS */


#if USE_BASE == 84 || defined(USE_TETRAOCTAGESIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_TRIOCTAGESIMAL_NUMBERS
		#define USE_TRIOCTAGESIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_83(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 83
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_82
		#define _inc_82 83
	#endif
	#ifndef _inc_83
		#define _inc_83 0
	#endif
	#ifndef _dec_0
		#define _dec_0 83
	#endif
	#define _dec_83 82

	/* logic */
	#define IS_83_OR_LESS(x) OR(IS_82_OR_LESS(x),EQUAL(83,x))

	/* looping */
	#define PROTO_FROM_83(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,83)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),83),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),83)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,83,do_macro(83,__VA_ARGS__))

#endif /* BASE-84 : USE_TETRAOCTAGESIMAL_NUMBERS */


#if USE_BASE == 83 || defined(USE_TRIOCTAGESIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_DUOOCTAGESIMAL_NUMBERS
		#define USE_DUOOCTAGESIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_82(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 82
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_81
		#define _inc_81 82
	#endif
	#ifndef _inc_82
		#define _inc_82 0
	#endif
	#ifndef _dec_0
		#define _dec_0 82
	#endif
	#define _dec_82 81

	/* logic */
	#define IS_82_OR_LESS(x) OR(IS_81_OR_LESS(x),EQUAL(82,x))

	/* looping */
	#define PROTO_FROM_82(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,82)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),82),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),82)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,82,do_macro(82,__VA_ARGS__))

#endif /* BASE-83 : USE_TRIOCTAGESIMAL_NUMBERS */


#if USE_BASE == 82 || defined(USE_DUOOCTAGESIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_UNOCTAGESIMAL_NUMBERS
		#define USE_UNOCTAGESIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_81(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 81
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_80
		#define _inc_80 81
	#endif
	#ifndef _inc_81
		#define _inc_81 0
	#endif
	#ifndef _dec_0
		#define _dec_0 81
	#endif
	#define _dec_81 80

	/* logic */
	#define IS_81_OR_LESS(x) OR(IS_80_OR_LESS(x),EQUAL(81,x))

	/* looping */
	#define PROTO_FROM_81(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,81)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),81),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),81)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,81,do_macro(81,__VA_ARGS__))

#endif /* BASE-82 : USE_DUOOCTAGESIMAL_NUMBERS */


#if USE_BASE == 81 || defined(USE_UNOCTAGESIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_OCTAGESIMAL_NUMBERS
		#define USE_OCTAGESIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_80(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 80
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_79
		#define _inc_79 80
	#endif
	#ifndef _inc_80
		#define _inc_80 0
	#endif
	#ifndef _dec_0
		#define _dec_0 80
	#endif
	#define _dec_80 79

	/* logic */
	#define IS_80_OR_LESS(x) OR(IS_79_OR_LESS(x),EQUAL(80,x))

	/* looping */
	#define PROTO_FROM_80(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,80)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),80),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),80)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,80,do_macro(80,__VA_ARGS__))

#endif /* BASE-81 : USE_UNOCTAGESIMAL_NUMBERS */


#if USE_BASE == 80 || defined(USE_OCTAGESIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_NONSPETAGESIMAL_NUMBERS
		#define USE_NONSPETAGESIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_79(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 79
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_78
		#define _inc_78 79
	#endif
	#ifndef _inc_79
		#define _inc_79 0
	#endif
	#ifndef _dec_0
		#define _dec_0 79
	#endif
	#define _dec_79 78

	/* logic */
	#define IS_79_OR_LESS(x) OR(IS_78_OR_LESS(x),EQUAL(79,x))

	/* looping */
	#define PROTO_FROM_79(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,79)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),79),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),79)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,79,do_macro(79,__VA_ARGS__))

#endif /* BASE-80 : USE_OCTAGESIMAL_NUMBERS */


#if USE_BASE == 79 || defined(USE_NONSPETAGESIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_OCTOSPETAGESIMAL_NUMBERS
		#define USE_OCTOSPETAGESIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_78(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 78
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_77
		#define _inc_77 78
	#endif
	#ifndef _inc_78
		#define _inc_78 0
	#endif
	#ifndef _dec_0
		#define _dec_0 78
	#endif
	#define _dec_78 77

	/* logic */
	#define IS_78_OR_LESS(x) OR(IS_77_OR_LESS(x),EQUAL(78,x))

	/* looping */
	#define PROTO_FROM_78(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,78)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),78),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),78)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,78,do_macro(78,__VA_ARGS__))

#endif /* BASE-79 : USE_NONSPETAGESIMAL_NUMBERS */


#if USE_BASE == 78 || defined(USE_OCTOSPETAGESIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_HEPTASPETAGESIMAL_NUMBERS
		#define USE_HEPTASPETAGESIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_77(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 77
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_76
		#define _inc_76 77
	#endif
	#ifndef _inc_77
		#define _inc_77 0
	#endif
	#ifndef _dec_0
		#define _dec_0 77
	#endif
	#define _dec_77 76

	/* logic */
	#define IS_77_OR_LESS(x) OR(IS_76_OR_LESS(x),EQUAL(77,x))

	/* looping */
	#define PROTO_FROM_77(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,77)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),77),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),77)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,77,do_macro(77,__VA_ARGS__))

#endif /* BASE-78 : USE_OCTOSPETAGESIMAL_NUMBERS */


#if USE_BASE == 77 || defined(USE_HEPTASPETAGESIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_HEXASPETAGESIMAL_NUMBERS
		#define USE_HEXASPETAGESIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_76(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 76
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_75
		#define _inc_75 76
	#endif
	#ifndef _inc_76
		#define _inc_76 0
	#endif
	#ifndef _dec_0
		#define _dec_0 76
	#endif
	#define _dec_76 75

	/* logic */
	#define IS_76_OR_LESS(x) OR(IS_75_OR_LESS(x),EQUAL(76,x))

	/* looping */
	#define PROTO_FROM_76(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,76)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),76),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),76)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,76,do_macro(76,__VA_ARGS__))

#endif /* BASE-77 : USE_HEPTASPETAGESIMAL_NUMBERS */


#if USE_BASE == 76 || defined(USE_HEXASPETAGESIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_PENTASPETAGESIMAL_NUMBERS
		#define USE_PENTASPETAGESIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_75(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 75
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_74
		#define _inc_74 75
	#endif
	#ifndef _inc_75
		#define _inc_75 0
	#endif
	#ifndef _dec_0
		#define _dec_0 75
	#endif
	#define _dec_75 74

	/* logic */
	#define IS_75_OR_LESS(x) OR(IS_74_OR_LESS(x),EQUAL(75,x))

	/* looping */
	#define PROTO_FROM_75(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,75)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),75),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),75)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,75,do_macro(75,__VA_ARGS__))

#endif /* BASE-76 : USE_HEXASPETAGESIMAL_NUMBERS */


#if USE_BASE == 75 || defined(USE_PENTASPETAGESIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_TETRASPETAGESIMAL_NUMBERS
		#define USE_TETRASPETAGESIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_74(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 74
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_73
		#define _inc_73 74
	#endif
	#ifndef _inc_74
		#define _inc_74 0
	#endif
	#ifndef _dec_0
		#define _dec_0 74
	#endif
	#define _dec_74 73

	/* logic */
	#define IS_74_OR_LESS(x) OR(IS_73_OR_LESS(x),EQUAL(74,x))

	/* looping */
	#define PROTO_FROM_74(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,74)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),74),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),74)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,74,do_macro(74,__VA_ARGS__))

#endif /* BASE-75 : USE_PENTASPETAGESIMAL_NUMBERS */


#if USE_BASE == 74 || defined(USE_TETRASPETAGESIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_TRISPETAGESIMAL_NUMBERS
		#define USE_TRISPETAGESIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_73(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 73
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_72
		#define _inc_72 73
	#endif
	#ifndef _inc_73
		#define _inc_73 0
	#endif
	#ifndef _dec_0
		#define _dec_0 73
	#endif
	#define _dec_73 72

	/* logic */
	#define IS_73_OR_LESS(x) OR(IS_72_OR_LESS(x),EQUAL(73,x))

	/* looping */
	#define PROTO_FROM_73(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,73)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),73),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),73)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,73,do_macro(73,__VA_ARGS__))

#endif /* BASE-74 : USE_TETRASPETAGESIMAL_NUMBERS */


#if USE_BASE == 73 || defined(USE_TRISPETAGESIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_DUOSPETAGESIMAL_NUMBERS
		#define USE_DUOSPETAGESIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_72(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 72
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_71
		#define _inc_71 72
	#endif
	#ifndef _inc_72
		#define _inc_72 0
	#endif
	#ifndef _dec_0
		#define _dec_0 72
	#endif
	#define _dec_72 71

	/* logic */
	#define IS_72_OR_LESS(x) OR(IS_71_OR_LESS(x),EQUAL(72,x))

	/* looping */
	#define PROTO_FROM_72(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,72)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),72),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),72)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,72,do_macro(72,__VA_ARGS__))

#endif /* BASE-73 : USE_TRISPETAGESIMAL_NUMBERS */


#if USE_BASE == 72 || defined(USE_DUOSPETAGESIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_UNSPETAGESIMAL_NUMBERS
		#define USE_UNSPETAGESIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_71(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 71
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_70
		#define _inc_70 71
	#endif
	#ifndef _inc_71
		#define _inc_71 0
	#endif
	#ifndef _dec_0
		#define _dec_0 71
	#endif
	#define _dec_71 70

	/* logic */
	#define IS_71_OR_LESS(x) OR(IS_70_OR_LESS(x),EQUAL(71,x))

	/* looping */
	#define PROTO_FROM_71(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,71)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),71),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),71)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,71,do_macro(71,__VA_ARGS__))

#endif /* BASE-72 : USE_DUOSPETAGESIMAL_NUMBERS */


#if USE_BASE == 71 || defined(USE_UNSPETAGESIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_SPETAGESIMAL_NUMBERS
		#define USE_SPETAGESIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_70(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 70
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_69
		#define _inc_69 70
	#endif
	#ifndef _inc_70
		#define _inc_70 0
	#endif
	#ifndef _dec_0
		#define _dec_0 70
	#endif
	#define _dec_70 69

	/* logic */
	#define IS_70_OR_LESS(x) OR(IS_69_OR_LESS(x),EQUAL(70,x))

	/* looping */
	#define PROTO_FROM_70(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,70)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),70),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),70)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,70,do_macro(70,__VA_ARGS__))

#endif /* BASE-71 : USE_UNSPETAGESIMAL_NUMBERS */


#if USE_BASE == 70 || defined(USE_SPETAGESIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_NONSEXAGESIMAL_NUMBERS
		#define USE_NONSEXAGESIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_69(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 69
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_68
		#define _inc_68 69
	#endif
	#ifndef _inc_69
		#define _inc_69 0
	#endif
	#ifndef _dec_0
		#define _dec_0 69
	#endif
	#define _dec_69 68

	/* logic */
	#define IS_69_OR_LESS(x) OR(IS_68_OR_LESS(x),EQUAL(69,x))

	/* looping */
	#define PROTO_FROM_69(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,69)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),69),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),69)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,69,do_macro(69,__VA_ARGS__))

#endif /* BASE-70 : USE_SPETAGESIMAL_NUMBERS */


#if USE_BASE == 69 || defined(USE_NONSEXAGESIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_OCTOSEXAGESIMAL_NUMBERS
		#define USE_OCTOSEXAGESIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_68(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 68
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_67
		#define _inc_67 68
	#endif
	#ifndef _inc_68
		#define _inc_68 0
	#endif
	#ifndef _dec_0
		#define _dec_0 68
	#endif
	#define _dec_68 67

	/* logic */
	#define IS_68_OR_LESS(x) OR(IS_67_OR_LESS(x),EQUAL(68,x))

	/* looping */
	#define PROTO_FROM_68(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,68)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),68),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),68)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,68,do_macro(68,__VA_ARGS__))

#endif /* BASE-69 : USE_NONSEXAGESIMAL_NUMBERS */


#if USE_BASE == 68 || defined(USE_OCTOSEXAGESIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_HEPTASEXAGESIMAL_NUMBERS
		#define USE_HEPTASEXAGESIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_67(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 67
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_66
		#define _inc_66 67
	#endif
	#ifndef _inc_67
		#define _inc_67 0
	#endif
	#ifndef _dec_0
		#define _dec_0 67
	#endif
	#define _dec_67 66

	/* logic */
	#define IS_67_OR_LESS(x) OR(IS_66_OR_LESS(x),EQUAL(67,x))

	/* looping */
	#define PROTO_FROM_67(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,67)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),67),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),67)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,67,do_macro(67,__VA_ARGS__))

#endif /* BASE-68 : USE_OCTOSEXAGESIMAL_NUMBERS */


#if USE_BASE == 67 || defined(USE_HEPTASEXAGESIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_HEXASEXAGESIMAL_NUMBERS
		#define USE_HEXASEXAGESIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_66(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 66
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_65
		#define _inc_65 66
	#endif
	#ifndef _inc_66
		#define _inc_66 0
	#endif
	#ifndef _dec_0
		#define _dec_0 66
	#endif
	#define _dec_66 65

	/* logic */
	#define IS_66_OR_LESS(x) OR(IS_65_OR_LESS(x),EQUAL(66,x))

	/* looping */
	#define PROTO_FROM_66(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,66)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),66),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),66)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,66,do_macro(66,__VA_ARGS__))

#endif /* BASE-67 : USE_HEPTASEXAGESIMAL_NUMBERS */


#if USE_BASE == 66 || defined(USE_HEXASEXAGESIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_PENTASEXAGESIMAL_NUMBERS
		#define USE_PENTASEXAGESIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_65(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 65
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_64
		#define _inc_64 65
	#endif
	#ifndef _inc_65
		#define _inc_65 0
	#endif
	#ifndef _dec_0
		#define _dec_0 65
	#endif
	#define _dec_65 64

	/* logic */
	#define IS_65_OR_LESS(x) OR(IS_64_OR_LESS(x),EQUAL(65,x))

	/* looping */
	#define PROTO_FROM_65(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,65)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),65),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),65)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,65,do_macro(65,__VA_ARGS__))

#endif /* BASE-66 : USE_HEXASEXAGESIMAL_NUMBERS */


#if USE_BASE == 65 || defined(USE_PENTASEXAGESIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_TETRASEXAGESIMAL_NUMBERS
		#define USE_TETRASEXAGESIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_64(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 64
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_63
		#define _inc_63 64
	#endif
	#ifndef _inc_64
		#define _inc_64 0
	#endif
	#ifndef _dec_0
		#define _dec_0 64
	#endif
	#define _dec_64 63

	/* logic */
	#define IS_64_OR_LESS(x) OR(IS_63_OR_LESS(x),EQUAL(64,x))

	/* looping */
	#define PROTO_FROM_64(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,64)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),64),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),64)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,64,do_macro(64,__VA_ARGS__))

#endif /* BASE-65 : USE_PENTASEXAGESIMAL_NUMBERS */


#if USE_BASE == 64 || defined(USE_TETRASEXAGESIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_TRISEXAGESIMAL_NUMBERS
		#define USE_TRISEXAGESIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_63(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 63
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_62
		#define _inc_62 63
	#endif
	#ifndef _inc_63
		#define _inc_63 0
	#endif
	#ifndef _dec_0
		#define _dec_0 63
	#endif
	#define _dec_63 62

	/* logic */
	#define IS_63_OR_LESS(x) OR(IS_62_OR_LESS(x),EQUAL(63,x))

	/* looping */
	#define PROTO_FROM_63(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,63)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),63),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),63)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,63,do_macro(63,__VA_ARGS__))

#endif /* BASE-64 : USE_TETRASEXAGESIMAL_NUMBERS */


#if USE_BASE == 63 || defined(USE_TRISEXAGESIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_DUOSEXAGESIMAL_NUMBERS
		#define USE_DUOSEXAGESIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_62(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 62
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_61
		#define _inc_61 62
	#endif
	#ifndef _inc_62
		#define _inc_62 0
	#endif
	#ifndef _dec_0
		#define _dec_0 62
	#endif
	#define _dec_62 61

	/* logic */
	#define IS_62_OR_LESS(x) OR(IS_61_OR_LESS(x),EQUAL(62,x))

	/* looping */
	#define PROTO_FROM_62(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,62)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),62),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),62)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,62,do_macro(62,__VA_ARGS__))

#endif /* BASE-63 : USE_TRISEXAGESIMAL_NUMBERS */


#if USE_BASE == 62 || defined(USE_DUOSEXAGESIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_UNSEXAGESIMAL_NUMBERS
		#define USE_UNSEXAGESIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_61(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 61
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_60
		#define _inc_60 61
	#endif
	#ifndef _inc_61
		#define _inc_61 0
	#endif
	#ifndef _dec_0
		#define _dec_0 61
	#endif
	#define _dec_61 60

	/* logic */
	#define IS_61_OR_LESS(x) OR(IS_60_OR_LESS(x),EQUAL(61,x))

	/* looping */
	#define PROTO_FROM_61(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,61)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),61),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),61)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,61,do_macro(61,__VA_ARGS__))

#endif /* BASE-62 : USE_DUOSEXAGESIMAL_NUMBERS */


#if USE_BASE == 61 || defined(USE_UNSEXAGESIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_SEXAGESIMAL_NUMBERS
		#define USE_SEXAGESIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_60(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 60
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_59
		#define _inc_59 60
	#endif
	#ifndef _inc_60
		#define _inc_60 0
	#endif
	#ifndef _dec_0
		#define _dec_0 60
	#endif
	#define _dec_60 59

	/* logic */
	#define IS_60_OR_LESS(x) OR(IS_59_OR_LESS(x),EQUAL(60,x))

	/* looping */
	#define PROTO_FROM_60(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,60)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),60),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),60)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,60,do_macro(60,__VA_ARGS__))

#endif /* BASE-61 : USE_UNSEXAGESIMAL_NUMBERS */


#if USE_BASE == 60 || defined(USE_SEXAGESIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_NONGUINQUAGESIMAL_NUMBERS
		#define USE_NONGUINQUAGESIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_59(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 59
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_58
		#define _inc_58 59
	#endif
	#ifndef _inc_59
		#define _inc_59 0
	#endif
	#ifndef _dec_0
		#define _dec_0 59
	#endif
	#define _dec_59 58

	/* logic */
	#define IS_59_OR_LESS(x) OR(IS_58_OR_LESS(x),EQUAL(59,x))

	/* looping */
	#define PROTO_FROM_59(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,59)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),59),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),59)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,59,do_macro(59,__VA_ARGS__))

#endif /* BASE-60 : USE_SEXAGESIMAL_NUMBERS */


#if USE_BASE == 59 || defined(USE_NONGUINQUAGESIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_OCTOGUINQUAGESIMAL_NUMBERS
		#define USE_OCTOGUINQUAGESIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_58(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 58
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_57
		#define _inc_57 58
	#endif
	#ifndef _inc_58
		#define _inc_58 0
	#endif
	#ifndef _dec_0
		#define _dec_0 58
	#endif
	#define _dec_58 57

	/* logic */
	#define IS_58_OR_LESS(x) OR(IS_57_OR_LESS(x),EQUAL(58,x))

	/* looping */
	#define PROTO_FROM_58(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,58)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),58),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),58)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,58,do_macro(58,__VA_ARGS__))

#endif /* BASE-59 : USE_NONGUINQUAGESIMAL_NUMBERS */


#if USE_BASE == 58 || defined(USE_OCTOGUINQUAGESIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_HEPTAGUINQUAGESIMAL_NUMBERS
		#define USE_HEPTAGUINQUAGESIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_57(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 57
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_56
		#define _inc_56 57
	#endif
	#ifndef _inc_57
		#define _inc_57 0
	#endif
	#ifndef _dec_0
		#define _dec_0 57
	#endif
	#define _dec_57 56

	/* logic */
	#define IS_57_OR_LESS(x) OR(IS_56_OR_LESS(x),EQUAL(57,x))

	/* looping */
	#define PROTO_FROM_57(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,57)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),57),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),57)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,57,do_macro(57,__VA_ARGS__))

#endif /* BASE-58 : USE_OCTOGUINQUAGESIMAL_NUMBERS */


#if USE_BASE == 57 || defined(USE_HEPTAGUINQUAGESIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_HEXAGUINQUAGESIMAL_NUMBERS
		#define USE_HEXAGUINQUAGESIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_56(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 56
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_55
		#define _inc_55 56
	#endif
	#ifndef _inc_56
		#define _inc_56 0
	#endif
	#ifndef _dec_0
		#define _dec_0 56
	#endif
	#define _dec_56 55

	/* logic */
	#define IS_56_OR_LESS(x) OR(IS_55_OR_LESS(x),EQUAL(56,x))

	/* looping */
	#define PROTO_FROM_56(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,56)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),56),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),56)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,56,do_macro(56,__VA_ARGS__))

#endif /* BASE-57 : USE_HEPTAGUINQUAGESIMAL_NUMBERS */


#if USE_BASE == 56 || defined(USE_HEXAGUINQUAGESIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_PENTAGUINQUAGESIMAL_NUMBERS
		#define USE_PENTAGUINQUAGESIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_55(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 55
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_54
		#define _inc_54 55
	#endif
	#ifndef _inc_55
		#define _inc_55 0
	#endif
	#ifndef _dec_0
		#define _dec_0 55
	#endif
	#define _dec_55 54

	/* logic */
	#define IS_55_OR_LESS(x) OR(IS_54_OR_LESS(x),EQUAL(55,x))

	/* looping */
	#define PROTO_FROM_55(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,55)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),55),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),55)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,55,do_macro(55,__VA_ARGS__))

#endif /* BASE-56 : USE_HEXAGUINQUAGESIMAL_NUMBERS */


#if USE_BASE == 55 || defined(USE_PENTAGUINQUAGESIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_TETRAGUINQUAGESIMAL_NUMBERS
		#define USE_TETRAGUINQUAGESIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_54(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 54
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_53
		#define _inc_53 54
	#endif
	#ifndef _inc_54
		#define _inc_54 0
	#endif
	#ifndef _dec_0
		#define _dec_0 54
	#endif
	#define _dec_54 53

	/* logic */
	#define IS_54_OR_LESS(x) OR(IS_53_OR_LESS(x),EQUAL(54,x))

	/* looping */
	#define PROTO_FROM_54(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,54)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),54),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),54)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,54,do_macro(54,__VA_ARGS__))

#endif /* BASE-55 : USE_PENTAGUINQUAGESIMAL_NUMBERS */


#if USE_BASE == 54 || defined(USE_TETRAGUINQUAGESIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_TRIGUINQUAGESIMAL_NUMBERS
		#define USE_TRIGUINQUAGESIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_53(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 53
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_52
		#define _inc_52 53
	#endif
	#ifndef _inc_53
		#define _inc_53 0
	#endif
	#ifndef _dec_0
		#define _dec_0 53
	#endif
	#define _dec_53 52

	/* logic */
	#define IS_53_OR_LESS(x) OR(IS_52_OR_LESS(x),EQUAL(53,x))

	/* looping */
	#define PROTO_FROM_53(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,53)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),53),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),53)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,53,do_macro(53,__VA_ARGS__))

#endif /* BASE-54 : USE_TETRAGUINQUAGESIMAL_NUMBERS */


#if USE_BASE == 53 || defined(USE_TRIGUINQUAGESIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_DUOGUINQUAGESIMAL_NUMBERS
		#define USE_DUOGUINQUAGESIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_52(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 52
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_51
		#define _inc_51 52
	#endif
	#ifndef _inc_52
		#define _inc_52 0
	#endif
	#ifndef _dec_0
		#define _dec_0 52
	#endif
	#define _dec_52 51

	/* logic */
	#define IS_52_OR_LESS(x) OR(IS_51_OR_LESS(x),EQUAL(52,x))

	/* looping */
	#define PROTO_FROM_52(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,52)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),52),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),52)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,52,do_macro(52,__VA_ARGS__))

#endif /* BASE-53 : USE_TRIGUINQUAGESIMAL_NUMBERS */


#if USE_BASE == 52 || defined(USE_DUOGUINQUAGESIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_UNGUINQUAGESIMAL_NUMBERS
		#define USE_UNGUINQUAGESIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_51(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 51
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_50
		#define _inc_50 51
	#endif
	#ifndef _inc_51
		#define _inc_51 0
	#endif
	#ifndef _dec_0
		#define _dec_0 51
	#endif
	#define _dec_51 50

	/* logic */
	#define IS_51_OR_LESS(x) OR(IS_50_OR_LESS(x),EQUAL(51,x))

	/* looping */
	#define PROTO_FROM_51(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,51)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),51),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),51)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,51,do_macro(51,__VA_ARGS__))

#endif /* BASE-52 : USE_DUOGUINQUAGESIMAL_NUMBERS */


#if USE_BASE == 51 || defined(USE_UNGUINQUAGESIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_GUINQUAGESIMAL_NUMBERS
		#define USE_GUINQUAGESIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_50(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 50
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_49
		#define _inc_49 50
	#endif
	#ifndef _inc_50
		#define _inc_50 0
	#endif
	#ifndef _dec_0
		#define _dec_0 50
	#endif
	#define _dec_50 49

	/* logic */
	#define IS_50_OR_LESS(x) OR(IS_49_OR_LESS(x),EQUAL(50,x))

	/* looping */
	#define PROTO_FROM_50(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,50)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),50),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),50)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,50,do_macro(50,__VA_ARGS__))

#endif /* BASE-51 : USE_UNGUINQUAGESIMAL_NUMBERS */


#if USE_BASE == 50 || defined(USE_GUINQUAGESIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_NONQUADRAGESIMAL_NUMBERS
		#define USE_NONQUADRAGESIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_49(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 49
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_48
		#define _inc_48 49
	#endif
	#ifndef _inc_49
		#define _inc_49 0
	#endif
	#ifndef _dec_0
		#define _dec_0 49
	#endif
	#define _dec_49 48

	/* logic */
	#define IS_49_OR_LESS(x) OR(IS_48_OR_LESS(x),EQUAL(49,x))

	/* looping */
	#define PROTO_FROM_49(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,49)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),49),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),49)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,49,do_macro(49,__VA_ARGS__))

#endif /* BASE-50 : USE_GUINQUAGESIMAL_NUMBERS */


#if USE_BASE == 49 || defined(USE_NONQUADRAGESIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_OCTOQUADRAGESIMAL_NUMBERS
		#define USE_OCTOQUADRAGESIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_48(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 48
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_47
		#define _inc_47 48
	#endif
	#ifndef _inc_48
		#define _inc_48 0
	#endif
	#ifndef _dec_0
		#define _dec_0 48
	#endif
	#define _dec_48 47

	/* logic */
	#define IS_48_OR_LESS(x) OR(IS_47_OR_LESS(x),EQUAL(48,x))

	/* looping */
	#define PROTO_FROM_48(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,48)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),48),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),48)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,48,do_macro(48,__VA_ARGS__))

#endif /* BASE-49 : USE_NONQUADRAGESIMAL_NUMBERS */


#if USE_BASE == 48 || defined(USE_OCTOQUADRAGESIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_HEPTAQUADRAGESIMAL_NUMBERS
		#define USE_HEPTAQUADRAGESIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_47(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 47
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_46
		#define _inc_46 47
	#endif
	#ifndef _inc_47
		#define _inc_47 0
	#endif
	#ifndef _dec_0
		#define _dec_0 47
	#endif
	#define _dec_47 46

	/* logic */
	#define IS_47_OR_LESS(x) OR(IS_46_OR_LESS(x),EQUAL(47,x))

	/* looping */
	#define PROTO_FROM_47(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,47)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),47),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),47)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,47,do_macro(47,__VA_ARGS__))

#endif /* BASE-48 : USE_OCTOQUADRAGESIMAL_NUMBERS */


#if USE_BASE == 47 || defined(USE_HEPTAQUADRAGESIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_HEXAQUADRAGESIMAL_NUMBERS
		#define USE_HEXAQUADRAGESIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_46(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 46
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_45
		#define _inc_45 46
	#endif
	#ifndef _inc_46
		#define _inc_46 0
	#endif
	#ifndef _dec_0
		#define _dec_0 46
	#endif
	#define _dec_46 45

	/* logic */
	#define IS_46_OR_LESS(x) OR(IS_45_OR_LESS(x),EQUAL(46,x))

	/* looping */
	#define PROTO_FROM_46(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,46)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),46),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),46)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,46,do_macro(46,__VA_ARGS__))

#endif /* BASE-47 : USE_HEPTAQUADRAGESIMAL_NUMBERS */


#if USE_BASE == 46 || defined(USE_HEXAQUADRAGESIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_PENTAQUADRAGESIMAL_NUMBERS
		#define USE_PENTAQUADRAGESIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_45(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 45
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_44
		#define _inc_44 45
	#endif
	#ifndef _inc_45
		#define _inc_45 0
	#endif
	#ifndef _dec_0
		#define _dec_0 45
	#endif
	#define _dec_45 44

	/* logic */
	#define IS_45_OR_LESS(x) OR(IS_44_OR_LESS(x),EQUAL(45,x))

	/* looping */
	#define PROTO_FROM_45(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,45)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),45),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),45)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,45,do_macro(45,__VA_ARGS__))

#endif /* BASE-46 : USE_HEXAQUADRAGESIMAL_NUMBERS */


#if USE_BASE == 45 || defined(USE_PENTAQUADRAGESIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_TETRAQUADRAGESIMAL_NUMBERS
		#define USE_TETRAQUADRAGESIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_44(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 44
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_43
		#define _inc_43 44
	#endif
	#ifndef _inc_44
		#define _inc_44 0
	#endif
	#ifndef _dec_0
		#define _dec_0 44
	#endif
	#define _dec_44 43

	/* logic */
	#define IS_44_OR_LESS(x) OR(IS_43_OR_LESS(x),EQUAL(44,x))

	/* looping */
	#define PROTO_FROM_44(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,44)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),44),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),44)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,44,do_macro(44,__VA_ARGS__))

#endif /* BASE-45 : USE_PENTAQUADRAGESIMAL_NUMBERS */


#if USE_BASE == 44 || defined(USE_TETRAQUADRAGESIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_TRIQUADRAGESIMAL_NUMBERS
		#define USE_TRIQUADRAGESIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_43(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 43
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_42
		#define _inc_42 43
	#endif
	#ifndef _inc_43
		#define _inc_43 0
	#endif
	#ifndef _dec_0
		#define _dec_0 43
	#endif
	#define _dec_43 42

	/* logic */
	#define IS_43_OR_LESS(x) OR(IS_42_OR_LESS(x),EQUAL(43,x))

	/* looping */
	#define PROTO_FROM_43(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,43)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),43),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),43)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,43,do_macro(43,__VA_ARGS__))

#endif /* BASE-44 : USE_TETRAQUADRAGESIMAL_NUMBERS */


#if USE_BASE == 43 || defined(USE_TRIQUADRAGESIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_DUOQUADRAGESIMAL_NUMBERS
		#define USE_DUOQUADRAGESIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_42(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 42
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_41
		#define _inc_41 42
	#endif
	#ifndef _inc_42
		#define _inc_42 0
	#endif
	#ifndef _dec_0
		#define _dec_0 42
	#endif
	#define _dec_42 41

	/* logic */
	#define IS_42_OR_LESS(x) OR(IS_41_OR_LESS(x),EQUAL(42,x))

	/* looping */
	#define PROTO_FROM_42(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,42)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),42),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),42)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,42,do_macro(42,__VA_ARGS__))

#endif /* BASE-43 : USE_TRIQUADRAGESIMAL_NUMBERS */


#if USE_BASE == 42 || defined(USE_DUOQUADRAGESIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_UNQUADRAGESIMAL_NUMBERS
		#define USE_UNQUADRAGESIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_41(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 41
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_40
		#define _inc_40 41
	#endif
	#ifndef _inc_41
		#define _inc_41 0
	#endif
	#ifndef _dec_0
		#define _dec_0 41
	#endif
	#define _dec_41 40

	/* logic */
	#define IS_41_OR_LESS(x) OR(IS_40_OR_LESS(x),EQUAL(41,x))

	/* looping */
	#define PROTO_FROM_41(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,41)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),41),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),41)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,41,do_macro(41,__VA_ARGS__))

#endif /* BASE-42 : USE_DUOQUADRAGESIMAL_NUMBERS */


#if USE_BASE == 41 || defined(USE_UNQUADRAGESIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_QUADRAGESIMAL_NUMBERS
		#define USE_QUADRAGESIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_40(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 40
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_39
		#define _inc_39 40
	#endif
	#ifndef _inc_40
		#define _inc_40 0
	#endif
	#ifndef _dec_0
		#define _dec_0 40
	#endif
	#define _dec_40 39

	/* logic */
	#define IS_40_OR_LESS(x) OR(IS_39_OR_LESS(x),EQUAL(40,x))

	/* looping */
	#define PROTO_FROM_40(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,40)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),40),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),40)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,40,do_macro(40,__VA_ARGS__))

#endif /* BASE-41 : USE_UNQUADRAGESIMAL_NUMBERS */


#if USE_BASE == 40 || defined(USE_QUADRAGESIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_NONTRIGESIMAL_NUMBERS
		#define USE_NONTRIGESIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_39(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 39
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_38
		#define _inc_38 39
	#endif
	#ifndef _inc_39
		#define _inc_39 0
	#endif
	#ifndef _dec_0
		#define _dec_0 39
	#endif
	#define _dec_39 38

	/* logic */
	#define IS_39_OR_LESS(x) OR(IS_38_OR_LESS(x),EQUAL(39,x))

	/* looping */
	#define PROTO_FROM_39(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,39)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),39),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),39)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,39,do_macro(39,__VA_ARGS__))

#endif /* BASE-40 : USE_QUADRAGESIMAL_NUMBERS */


#if USE_BASE == 39 || defined(USE_NONTRIGESIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_OCTOTRIGESIMAL_NUMBERS
		#define USE_OCTOTRIGESIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_38(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 38
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_37
		#define _inc_37 38
	#endif
	#ifndef _inc_38
		#define _inc_38 0
	#endif
	#ifndef _dec_0
		#define _dec_0 38
	#endif
	#define _dec_38 37

	/* logic */
	#define IS_38_OR_LESS(x) OR(IS_37_OR_LESS(x),EQUAL(38,x))

	/* looping */
	#define PROTO_FROM_38(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,38)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),38),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),38)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,38,do_macro(38,__VA_ARGS__))

#endif /* BASE-39 : USE_NONTRIGESIMAL_NUMBERS */


#if USE_BASE == 38 || defined(USE_OCTOTRIGESIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_HEPTATRIGESIMAL_NUMBERS
		#define USE_HEPTATRIGESIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_37(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 37
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_36
		#define _inc_36 37
	#endif
	#ifndef _inc_37
		#define _inc_37 0
	#endif
	#ifndef _dec_0
		#define _dec_0 37
	#endif
	#define _dec_37 36

	/* logic */
	#define IS_37_OR_LESS(x) OR(IS_36_OR_LESS(x),EQUAL(37,x))

	/* looping */
	#define PROTO_FROM_37(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,37)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),37),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),37)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,37,do_macro(37,__VA_ARGS__))

#endif /* BASE-38 : USE_OCTOTRIGESIMAL_NUMBERS */


#if USE_BASE == 37 || defined(USE_HEPTATRIGESIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_HEXATRIGESIMAL_NUMBERS
		#define USE_HEXATRIGESIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_36(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 36
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_35
		#define _inc_35 36
	#endif
	#ifndef _inc_36
		#define _inc_36 0
	#endif
	#ifndef _dec_0
		#define _dec_0 36
	#endif
	#define _dec_36 35

	/* logic */
	#define IS_36_OR_LESS(x) OR(IS_35_OR_LESS(x),EQUAL(36,x))

	/* looping */
	#define PROTO_FROM_36(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,36)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),36),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),36)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,36,do_macro(36,__VA_ARGS__))

#endif /* BASE-37 : USE_HEPTATRIGESIMAL_NUMBERS */


#if USE_BASE == 36 || defined(USE_HEXATRIGESIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_PENTATRIGESIMAL_NUMBERS
		#define USE_PENTATRIGESIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_35(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 35
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_34
		#define _inc_34 35
	#endif
	#ifndef _inc_35
		#define _inc_35 0
	#endif
	#ifndef _dec_0
		#define _dec_0 35
	#endif
	#define _dec_35 34

	/* logic */
	#define IS_35_OR_LESS(x) OR(IS_34_OR_LESS(x),EQUAL(35,x))

	/* looping */
	#define PROTO_FROM_35(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,35)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),35),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),35)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,35,do_macro(35,__VA_ARGS__))

#endif /* BASE-36 : USE_HEXATRIGESIMAL_NUMBERS */


#if USE_BASE == 35 || defined(USE_PENTATRIGESIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_TETRATRIGESIMAL_NUMBERS
		#define USE_TETRATRIGESIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_34(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 34
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_33
		#define _inc_33 34
	#endif
	#ifndef _inc_34
		#define _inc_34 0
	#endif
	#ifndef _dec_0
		#define _dec_0 34
	#endif
	#define _dec_34 33

	/* logic */
	#define IS_34_OR_LESS(x) OR(IS_33_OR_LESS(x),EQUAL(34,x))

	/* looping */
	#define PROTO_FROM_34(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,34)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),34),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),34)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,34,do_macro(34,__VA_ARGS__))

#endif /* BASE-35 : USE_PENTATRIGESIMAL_NUMBERS */


#if USE_BASE == 34 || defined(USE_TETRATRIGESIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_TRITRIGESIMAL_NUMBERS
		#define USE_TRITRIGESIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_33(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 33
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_32
		#define _inc_32 33
	#endif
	#ifndef _inc_33
		#define _inc_33 0
	#endif
	#ifndef _dec_0
		#define _dec_0 33
	#endif
	#define _dec_33 32

	/* logic */
	#define IS_33_OR_LESS(x) OR(IS_32_OR_LESS(x),EQUAL(33,x))

	/* looping */
	#define PROTO_FROM_33(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,33)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),33),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),33)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,33,do_macro(33,__VA_ARGS__))

#endif /* BASE-34 : USE_TETRATRIGESIMAL_NUMBERS */


#if USE_BASE == 33 || defined(USE_TRITRIGESIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_DUOTRIGESIMAL_NUMBERS
		#define USE_DUOTRIGESIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_32(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 32
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_31
		#define _inc_31 32
	#endif
	#ifndef _inc_32
		#define _inc_32 0
	#endif
	#ifndef _dec_0
		#define _dec_0 32
	#endif
	#define _dec_32 31

	/* logic */
	#define IS_32_OR_LESS(x) OR(IS_31_OR_LESS(x),EQUAL(32,x))

	/* looping */
	#define PROTO_FROM_32(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,32)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),32),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),32)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,32,do_macro(32,__VA_ARGS__))

#endif /* BASE-33 : USE_TRITRIGESIMAL_NUMBERS */


#if USE_BASE == 32 || defined(USE_DUOTRIGESIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_UNTRIGESIMAL_NUMBERS
		#define USE_UNTRIGESIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_31(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 31
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_30
		#define _inc_30 31
	#endif
	#ifndef _inc_31
		#define _inc_31 0
	#endif
	#ifndef _dec_0
		#define _dec_0 31
	#endif
	#define _dec_31 30

	/* logic */
	#define IS_31_OR_LESS(x) OR(IS_30_OR_LESS(x),EQUAL(31,x))

	/* looping */
	#define PROTO_FROM_31(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,31)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),31),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),31)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,31,do_macro(31,__VA_ARGS__))

#endif /* BASE-32 : USE_DUOTRIGESIMAL_NUMBERS */


#if USE_BASE == 31 || defined(USE_UNTRIGESIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_TRIGESIMAL_NUMBERS
		#define USE_TRIGESIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_30(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 30
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_29
		#define _inc_29 30
	#endif
	#ifndef _inc_30
		#define _inc_30 0
	#endif
	#ifndef _dec_0
		#define _dec_0 30
	#endif
	#define _dec_30 29

	/* logic */
	#define IS_30_OR_LESS(x) OR(IS_29_OR_LESS(x),EQUAL(30,x))

	/* looping */
	#define PROTO_FROM_30(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,30)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),30),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),30)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,30,do_macro(30,__VA_ARGS__))

#endif /* BASE-31 : USE_UNTRIGESIMAL_NUMBERS */


#if USE_BASE == 30 || defined(USE_TRIGESIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_NONVIGESIMAL_NUMBERS
		#define USE_NONVIGESIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_29(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 29
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_28
		#define _inc_28 29
	#endif
	#ifndef _inc_29
		#define _inc_29 0
	#endif
	#ifndef _dec_0
		#define _dec_0 29
	#endif
	#define _dec_29 28

	/* logic */
	#define IS_29_OR_LESS(x) OR(IS_28_OR_LESS(x),EQUAL(29,x))

	/* looping */
	#define PROTO_FROM_29(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,29)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),29),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),29)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,29,do_macro(29,__VA_ARGS__))

#endif /* BASE-30 : USE_TRIGESIMAL_NUMBERS */


#if USE_BASE == 29 || defined(USE_NONVIGESIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_OCTOVIGESIMAL_NUMBERS
		#define USE_OCTOVIGESIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_28(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 28
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_27
		#define _inc_27 28
	#endif
	#ifndef _inc_28
		#define _inc_28 0
	#endif
	#ifndef _dec_0
		#define _dec_0 28
	#endif
	#define _dec_28 27

	/* logic */
	#define IS_28_OR_LESS(x) OR(IS_27_OR_LESS(x),EQUAL(28,x))

	/* looping */
	#define PROTO_FROM_28(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,28)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),28),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),28)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,28,do_macro(28,__VA_ARGS__))

#endif /* BASE-29 : USE_NONVIGESIMAL_NUMBERS */


#if USE_BASE == 28 || defined(USE_OCTOVIGESIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_HEPTAVIGESIMAL_NUMBERS
		#define USE_HEPTAVIGESIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_27(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 27
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_26
		#define _inc_26 27
	#endif
	#ifndef _inc_27
		#define _inc_27 0
	#endif
	#ifndef _dec_0
		#define _dec_0 27
	#endif
	#define _dec_27 26

	/* logic */
	#define IS_27_OR_LESS(x) OR(IS_26_OR_LESS(x),EQUAL(27,x))

	/* looping */
	#define PROTO_FROM_27(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,27)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),27),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),27)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,27,do_macro(27,__VA_ARGS__))

#endif /* BASE-28 : USE_OCTOVIGESIMAL_NUMBERS */


#if USE_BASE == 27 || defined(USE_HEPTAVIGESIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_HEXAVIGESIMAL_NUMBERS
		#define USE_HEXAVIGESIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_26(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 26
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_25
		#define _inc_25 26
	#endif
	#ifndef _inc_26
		#define _inc_26 0
	#endif
	#ifndef _dec_0
		#define _dec_0 26
	#endif
	#define _dec_26 25

	/* logic */
	#define IS_26_OR_LESS(x) OR(IS_25_OR_LESS(x),EQUAL(26,x))

	/* looping */
	#define PROTO_FROM_26(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,26)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),26),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),26)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,26,do_macro(26,__VA_ARGS__))

#endif /* BASE-27 : USE_HEPTAVIGESIMAL_NUMBERS */


#if USE_BASE == 26 || defined(USE_HEXAVIGESIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_PENTAVIGESIMAL_NUMBERS
		#define USE_PENTAVIGESIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_25(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 25
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_24
		#define _inc_24 25
	#endif
	#ifndef _inc_25
		#define _inc_25 0
	#endif
	#ifndef _dec_0
		#define _dec_0 25
	#endif
	#define _dec_25 24

	/* logic */
	#define IS_25_OR_LESS(x) OR(IS_24_OR_LESS(x),EQUAL(25,x))

	/* looping */
	#define PROTO_FROM_25(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,25)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),25),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),25)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,25,do_macro(25,__VA_ARGS__))

#endif /* BASE-26 : USE_HEXAVIGESIMAL_NUMBERS */


#if USE_BASE == 25 || defined(USE_PENTAVIGESIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_TETRAVIGESIMAL_NUMBERS
		#define USE_TETRAVIGESIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_24(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 24
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_23
		#define _inc_23 24
	#endif
	#ifndef _inc_24
		#define _inc_24 0
	#endif
	#ifndef _dec_0
		#define _dec_0 24
	#endif
	#define _dec_24 23

	/* logic */
	#define IS_24_OR_LESS(x) OR(IS_23_OR_LESS(x),EQUAL(24,x))

	/* looping */
	#define PROTO_FROM_24(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,24)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),24),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),24)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,24,do_macro(24,__VA_ARGS__))

#endif /* BASE-25 : USE_PENTAVIGESIMAL_NUMBERS */


#if USE_BASE == 24 || defined(USE_TETRAVIGESIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_TRIVIGESIMAL_NUMBERS
		#define USE_TRIVIGESIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_23(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 23
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_22
		#define _inc_22 23
	#endif
	#ifndef _inc_23
		#define _inc_23 0
	#endif
	#ifndef _dec_0
		#define _dec_0 23
	#endif
	#define _dec_23 22

	/* logic */
	#define IS_23_OR_LESS(x) OR(IS_22_OR_LESS(x),EQUAL(23,x))

	/* looping */
	#define PROTO_FROM_23(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,23)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),23),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),23)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,23,do_macro(23,__VA_ARGS__))

#endif /* BASE-24 : USE_TETRAVIGESIMAL_NUMBERS */


#if USE_BASE == 23 || defined(USE_TRIVIGESIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_DUOVIGESIMAL_NUMBERS
		#define USE_DUOVIGESIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_22(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 22
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_21
		#define _inc_21 22
	#endif
	#ifndef _inc_22
		#define _inc_22 0
	#endif
	#ifndef _dec_0
		#define _dec_0 22
	#endif
	#define _dec_22 21

	/* logic */
	#define IS_22_OR_LESS(x) OR(IS_21_OR_LESS(x),EQUAL(22,x))

	/* looping */
	#define PROTO_FROM_22(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,22)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),22),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),22)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,22,do_macro(22,__VA_ARGS__))

#endif /* BASE-23 : USE_TRIVIGESIMAL_NUMBERS */


#if USE_BASE == 22 || defined(USE_DUOVIGESIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_UNVIGESIMAL_NUMBERS
		#define USE_UNVIGESIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_21(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 21
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_20
		#define _inc_20 21
	#endif
	#ifndef _inc_21
		#define _inc_21 0
	#endif
	#ifndef _dec_0
		#define _dec_0 21
	#endif
	#define _dec_21 20

	/* logic */
	#define IS_21_OR_LESS(x) OR(IS_20_OR_LESS(x),EQUAL(21,x))

	/* looping */
	#define PROTO_FROM_21(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,21)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),21),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),21)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,21,do_macro(21,__VA_ARGS__))

#endif /* BASE-22 : USE_DUOVIGESIMAL_NUMBERS */


#if USE_BASE == 21 || defined(USE_UNVIGESIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_VIGESIMAL_NUMBERS
		#define USE_VIGESIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_20(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 20
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_19
		#define _inc_19 20
	#endif
	#ifndef _inc_20
		#define _inc_20 0
	#endif
	#ifndef _dec_0
		#define _dec_0 20
	#endif
	#define _dec_20 19

	/* logic */
	#define IS_20_OR_LESS(x) OR(IS_19_OR_LESS(x),EQUAL(20,x))

	/* looping */
	#define PROTO_FROM_20(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,20)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),20),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),20)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,20,do_macro(20,__VA_ARGS__))

#endif /* BASE-21 : USE_UNVIGESIMAL_NUMBERS */


#if USE_BASE == 20 || defined(USE_VIGESIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_NONDECIMAL_NUMBERS
		#define USE_NONDECIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_19(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 19
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_18
		#define _inc_18 19
	#endif
	#ifndef _inc_19
		#define _inc_19 0
	#endif
	#ifndef _dec_0
		#define _dec_0 19
	#endif
	#define _dec_19 18

	/* logic */
	#define IS_19_OR_LESS(x) OR(IS_18_OR_LESS(x),EQUAL(19,x))

	/* looping */
	#define PROTO_FROM_19(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,19)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),19),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),19)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,19,do_macro(19,__VA_ARGS__))

#endif /* BASE-20 : USE_VIGESIMAL_NUMBERS */


#if USE_BASE == 19 || defined(USE_NONDECIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_OCTODECIMAL_NUMBERS
		#define USE_OCTODECIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_18(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 18
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_17
		#define _inc_17 18
	#endif
	#ifndef _inc_18
		#define _inc_18 0
	#endif
	#ifndef _dec_0
		#define _dec_0 18
	#endif
	#define _dec_18 17

	/* logic */
	#define IS_18_OR_LESS(x) OR(IS_17_OR_LESS(x),EQUAL(18,x))

	/* looping */
	#define PROTO_FROM_18(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,18)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),18),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),18)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,18,do_macro(18,__VA_ARGS__))

#endif /* BASE-19 : USE_NONDECIMAL_NUMBERS */


#if USE_BASE == 18 || defined(USE_OCTODECIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_HEPTADECIMAL_NUMBERS
		#define USE_HEPTADECIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_17(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 17
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_16
		#define _inc_16 17
	#endif
	#ifndef _inc_17
		#define _inc_17 0
	#endif
	#ifndef _dec_0
		#define _dec_0 17
	#endif
	#define _dec_17 16

	/* logic */
	#define IS_17_OR_LESS(x) OR(IS_16_OR_LESS(x),EQUAL(17,x))

	/* looping */
	#define PROTO_FROM_17(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,17)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),17),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),17)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,17,do_macro(17,__VA_ARGS__))

#endif /* BASE-18 : USE_OCTODECIMAL_NUMBERS */


#if USE_BASE == 17 || defined(USE_HEPTADECIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_HEXADECIMAL_NUMBERS
		#define USE_HEXADECIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_16(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 16
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_15
		#define _inc_15 16
	#endif
	#ifndef _inc_16
		#define _inc_16 0
	#endif
	#ifndef _dec_0
		#define _dec_0 16
	#endif
	#define _dec_16 15

	/* logic */
	#define IS_16_OR_LESS(x) OR(IS_15_OR_LESS(x),EQUAL(16,x))

	/* looping */
	#define PROTO_FROM_16(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,16)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),16),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),16)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,16,do_macro(16,__VA_ARGS__))

#endif /* BASE-17 : USE_HEPTADECIMAL_NUMBERS */



#endif /*CNLPPL_EXTENDED_PLACES_H*/
