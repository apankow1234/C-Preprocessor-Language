/*
	Andrew Pankow
	Language c
*/
#ifndef CNLPPL_EXTENDED_PLACES_H
#define CNLPPL_EXTENDED_PLACES_H
/*
	C's Preprocessor Language - Extended Place-Values


	This header extends the initial header into further place-values and can easily
	be appended to for continuation down the rabbit hole.

	I discovered while putting this together that it would be very easy to make this into
	a base-n implementation simply by defining the COMPARE_#, _inc_#, and _dec_# variables
	as well as the IS_#_OR_LESS() macros for whichever base you want. I will be creating a
	file for extended number systems... although, why you would want them in the preprocessor
	is beyond me... yet, I am the one who is curious, so it shall be.

	<TEMPLATE>
	Adding a new place-value will be as simple as copy/paste the block for a non-1's place
	and adding a zero to the ends of all of the _1#s
	* regex easy increment --> search  /([\_]1)([0]*s)/  --> replace  /\10\2/
*/

//SET THIS TO WHATEVER YOU WANT... SO LONG AS YOU'VE CREATED IT
#undef LARGEST_USABLE_PV
	#define LARGEST_USABLE_PV _1000s

/*
	PLACE-VALUE: 100's
	(the 3rd place-value)
*/
#define USE_100s
#if LARGEST_USABLE_PV == _100s || defined(USE_100s)
	#define _100s(x) x /*3rd place-value (base-10's hundreds' place)*/
	/*header*/
	#undef _pv_inc_10s
		#define _pv_inc_10s _100s
	#ifndef USE_10s
		#define USE_10s /*Needs previous place-value to do anything*/
	#endif
	#define _pv_dec_100s _10s
	#define _pv_inc_100s 0
	#define _pv_pop_100s(x,...) CLEAN(_pv_pop,_pv_dec_100s)(__VA_ARGS__) /**/
	#define _pv_100s() UP(CLEAN(_pv,_pv_dec_100s)())

	/*body*/
	/* constructor */
	#define _build_100s(...) \
		CLEAN(_build,_pv_dec_100s)(__VA_ARGS__) \
		,IF(IS_BLANK(_get_100s(__VA_ARGS__)))(0,_get_100s(__VA_ARGS__))
	#define _rbuild_100s(...) \
		IF(IS_BLANK(_get_100s(__VA_ARGS__)))(0,_get_100s(__VA_ARGS__)) \
		,CLEAN(_rbuild,_pv_dec_100s)(__VA_ARGS__)

	/* cycling through numbers/rolling over */
	#define _get_100s(x,...) CLEAN(_get,_pv_dec_100s)(__VA_ARGS__) /*Little-endian sequences*/
	#define _after_100s(x,...) CLEAN(_after,_pv_dec_100s)(__VA_ARGS__) /*Little-endian sequences*/
	#define _raster_100s(...) /*Big-endian final concatenation*/ \
		IF(IS_BLANK(_get_100s(__VA_ARGS__))) \
		( \
			CLEAN(_raster,_pv_dec_100s)(__VA_ARGS__) \
			,CLEAN(_get_100s(__VA_ARGS__),CLEAN(_raster,_pv_dec_100s)(__VA_ARGS__)) \
		)
	/*checks to make sure that the place-value was even used first and then cycles*/
	#define _cycle_100s(__up__,...) \
		IF(IS_BLANK(_get_100s(__VA_ARGS__))) \
		( \
			CLEAN(_cycle,_pv_dec_100s)(__up__,__VA_ARGS__) \
			,RUN(CLEAN(_cycle,_pv_dec_100s)(__up__,__VA_ARGS__),_cycle(_100s,__up__,__VA_ARGS__)) \
		)
	/*checks to see if this place-value will cycle up or down*/
	#define _will_cycle_100s(__up__,...) \
		AND( \
			CLEAN(_will_cycle,_pv_dec_100s)(__up__,__VA_ARGS__) \
			,WILL_ROLLOVER(_pv_dec_100s,__up__,__VA_ARGS__) \
		)

	/* comparison - for between and in-between, min and max can be switched around */
	#define _ne_100s(...) AND(CLEAN(_ne,_pv_dec_100s)(__VA_ARGS__),_ne(_100s,__VA_ARGS__))
	#define _eq_100s(...) AND(CLEAN(_eq,_pv_dec_100s)(__VA_ARGS__),_eq(_100s,__VA_ARGS__))
	#define _le_100s(...) IF(_le(_100s,__VA_ARGS__))(1,CLEAN(_le,_pv_dec_100s)(__VA_ARGS__))
	#define _lt_100s(...) IF(_lt(_100s,__VA_ARGS__))(1,CLEAN(_lt,_pv_dec_100s)(__VA_ARGS__))
	#define _ge_100s(...) IF(_ge(_100s,__VA_ARGS__))(1,CLEAN(_ge,_pv_dec_100s)(__VA_ARGS__))
	#define _gt_100s(...) IF(_gt(_100s,__VA_ARGS__))(1,CLEAN(_gt,_pv_dec_100s)(__VA_ARGS__))
	#define _bt_100s(...) IF(_bt(_100s,__VA_ARGS__))(1,CLEAN(_bt,_pv_dec_100s)(__VA_ARGS__))
	#define _ib_100s(...) IF(_ib(_100s,__VA_ARGS__))(1,CLEAN(_ib,_pv_dec_100s)(__VA_ARGS__))
#endif /* PLACE-VALUE: 100's */

/*
	PLACE-VALUE: 1000's
	(the 4th place-value)
*/
#define USE_1000s
#if LARGEST_USABLE_PV == _1000s || defined(USE_1000s)
	#define _1000s(x) x // 4th place-value (base-10's thousands' place)
	/*header*/
	#undef _pv_inc_100s
		#define _pv_inc_100s _1000s
	#ifndef USE_100s
		#define USE_100s /*Needs previous place-value to do anything*/
	#endif /*USE_100s*/
	#define _pv_dec_1000s _100s
	#define _pv_inc_1000s 0
	#define _pv_pop_1000s(x,...) CLEAN(_pv_pop,_pv_dec_1000s)(__VA_ARGS__) /**/
	#define _pv_1000s() UP(CLEAN(_pv,_pv_dec_1000s)())

	/*body*/
	/* constructor */
	#define _build_1000s(...) \
		CLEAN(_build,_pv_dec_1000s)(__VA_ARGS__) \
		,IF(IS_BLANK(_get_1000s(__VA_ARGS__)))(0,_get_1000s(__VA_ARGS__))
	#define _rbuild_1000s(...) \
		IF(IS_BLANK(_get_1000s(__VA_ARGS__)))(0,_get_1000s(__VA_ARGS__)) \
		,CLEAN(_rbuild,_pv_dec_1000s)(__VA_ARGS__)

	/* cycling through numbers/rolling over */
	#define _get_1000s(x,...) CLEAN(_get,_pv_dec_1000s)(__VA_ARGS__) /*Little-endian sequences*/
	#define _after_1000s(x,...) CLEAN(_after,_pv_dec_1000s)(__VA_ARGS__) /*Little-endian sequences*/
	#define _raster_1000s(...) /*Big-endian final concatenation*/ \
		IF(IS_BLANK(_get_1000s(__VA_ARGS__))) \
		( \
			CLEAN(_raster,_pv_dec_1000s)(__VA_ARGS__) \
			,CLEAN(_get_1000s(__VA_ARGS__),CLEAN(_raster,_pv_dec_1000s)(__VA_ARGS__)) \
		)
	/*checks to make sure that the place-value was even used first and then cycles*/
	#define _cycle_1000s(__up__,...) \
		IF(IS_BLANK(_get_1000s(__VA_ARGS__))) \
		( \
			CLEAN(_cycle,_pv_dec_1000s)(__up__,__VA_ARGS__) \
			,RUN(CLEAN(_cycle,_pv_dec_1000s)(__up__,__VA_ARGS__),_cycle(_1000s,__up__,__VA_ARGS__)) \
		)
	/*checks to see if this place-value will cycle up or down*/
	#define _will_cycle_1000s(__up__,...) \
		AND( \
			CLEAN(_will_cycle,_pv_dec_1000s)(__up__,__VA_ARGS__) \
			,WILL_ROLLOVER(_pv_dec_1000s,__up__,__VA_ARGS__) \
		)

	/* comparison - for between and in-between, min and max can be switched around */
	#define _ne_1000s(...) AND(CLEAN(_ne,_pv_dec_1000s)(__VA_ARGS__),_ne(_1000s,__VA_ARGS__))
	#define _eq_1000s(...) AND(CLEAN(_eq,_pv_dec_1000s)(__VA_ARGS__),_eq(_1000s,__VA_ARGS__))
	#define _le_1000s(...) IF(_le(_1000s,__VA_ARGS__))(1,CLEAN(_le,_pv_dec_1000s)(__VA_ARGS__))
	#define _lt_1000s(...) IF(_lt(_1000s,__VA_ARGS__))(1,CLEAN(_lt,_pv_dec_1000s)(__VA_ARGS__))
	#define _ge_1000s(...) IF(_ge(_1000s,__VA_ARGS__))(1,CLEAN(_ge,_pv_dec_1000s)(__VA_ARGS__))
	#define _gt_1000s(...) IF(_gt(_1000s,__VA_ARGS__))(1,CLEAN(_gt,_pv_dec_1000s)(__VA_ARGS__))
	#define _bt_1000s(...) IF(_bt(_1000s,__VA_ARGS__))(1,CLEAN(_bt,_pv_dec_1000s)(__VA_ARGS__))
	#define _ib_1000s(...) IF(_ib(_1000s,__VA_ARGS__))(1,CLEAN(_ib,_pv_dec_1000s)(__VA_ARGS__))
#endif /* PLACE-VALUE: 1000's */



#endif /*CNLPPL_EXTENDED_PLACES_H*/
