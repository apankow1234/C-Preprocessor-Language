/*
	Andrew Pankow
	Language c
*/
#ifndef CNLPPL_NUMBER_BASES_H
#define CNLPPL_NUMBER_BASES_H
/*
	C's Preprocessor Language - Numbers for the bases up to hexadecimal.
*/

#if USE_BASE > 16 && USE_BASE < 100
	/*
		Contains heptadecimal up to nonnonagesimal
		Easily extensible...
	*/
	#include "cppl_extended_bases.h"
#endif /* USE_BASE > 16 && USE_BASE < 100 */

/* #define USE_BINARY_NUMBERS             // base-2  */
/* #define USE_TERNARY_NUMBERS            // base-3  */
/* #define USE_QUATERNARY_NUMBERS         // base-4  */
/* #define USE_QUINARY_NUMBERS            // base-5  */
/* #define USE_SENARY_NUMBERS             // base-6  */
/* #define USE_SEPTENARY_NUMBERS          // base-7  */
/* #define USE_OCTAL_NUMBERS              // base-8  */
/* #define USE_NONARY_NUMBERS             // base-9  */
/* #define USE_DECIMAL_NUMBERS            // base-10 */
/* #define USE_UNDECIMAL_NUMBERS          // base-11 */
/* #define USE_DUODECIMAL_NUMBERS         // base-12 */
/* #define USE_TRIDECIMAL_NUMBERS         // base-13 */
/* #define USE_TETRADECIMAL_NUMBERS       // base-14 */
/* #define USE_PENTADECIMAL_NUMBERS       // base-15 */
/* #define USE_HEXADECIMAL_NUMBERS        // base-16 */


#if USE_BASE == 16 // Hex values as we're accustomed to
	#define COMPARE_A COMPARE_10
	#define COMPARE_a COMPARE_10
	#define COMPARE_B COMPARE_11
	#define COMPARE_b COMPARE_11
	#define COMPARE_C COMPARE_12
	#define COMPARE_c COMPARE_12
	#define COMPARE_D COMPARE_13
	#define COMPARE_d COMPARE_13
	#define COMPARE_E COMPARE_14
	#define COMPARE_e COMPARE_14
	#define COMPARE_F COMPARE_15
	#define COMPARE_f COMPARE_15
	#define __hex_val_A 10
	#define __hex_val_a 10
	#define __hex_val_B 11
	#define __hex_val_b 11
	#define __hex_val_C 12
	#define __hex_val_c 12
	#define __hex_val_D 13
	#define __hex_val_d 13
	#define __hex_val_E 14
	#define __hex_val_e 14
	#define __hex_val_F 15
	#define __hex_val_f 15
#endif /*Hex values as we're accustomed to*/

#if USE_BASE == 16 || defined(USE_HEXADECIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_PENTADECIMAL_NUMBERS
		#define USE_PENTADECIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_15(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 15
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_14
		#define _inc_14 15
	#endif
	#ifndef _inc_15
		#define _inc_15 0
	#endif
	#ifndef _dec_0
		#define _dec_0 15
	#endif
	#define _dec_15 14

	/* logic */
	#define IS_15_OR_LESS(x) OR(IS_14_OR_LESS(x),EQUAL(15,x))

	/* looping */
	#define PROTO_FROM_15(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,15)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),15),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),15)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,15,do_macro(15,__VA_ARGS__))

#endif /* BASE-16 : USE_HEXADECIMAL_NUMBERS */


#if USE_BASE == 15 || defined(USE_PENTADECIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_TETRADECIMAL_NUMBERS
		#define USE_TETRADECIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_14(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 14
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_13
		#define _inc_13 14
	#endif
	#ifndef _inc_14
		#define _inc_14 0
	#endif
	#ifndef _dec_0
		#define _dec_0 14
	#endif
	#define _dec_14 13

	/* logic */
	#define IS_14_OR_LESS(x) OR(IS_13_OR_LESS(x),EQUAL(14,x))

	/* looping */
	#define PROTO_FROM_14(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,14)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),14),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),14)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,14,do_macro(14,__VA_ARGS__))

#endif /* BASE-15 : USE_PENTADECIMAL_NUMBERS */


#if USE_BASE == 14 || defined(USE_TETRADECIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_TRIDECIMAL_NUMBERS
		#define USE_TRIDECIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_13(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 13
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_12
		#define _inc_12 13
	#endif
	#ifndef _inc_13
		#define _inc_13 0
	#endif
	#ifndef _dec_0
		#define _dec_0 13
	#endif
	#define _dec_13 12

	/* logic */
	#define IS_13_OR_LESS(x) OR(IS_12_OR_LESS(x),EQUAL(13,x))

	/* looping */
	#define PROTO_FROM_13(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,13)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),13),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),13)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,13,do_macro(13,__VA_ARGS__))

#endif /* BASE-14 : USE_TETRADECIMAL_NUMBERS */


#if USE_BASE == 13 || defined(USE_TRIDECIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_DUODECIMAL_NUMBERS
		#define USE_DUODECIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_12(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 12
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_11
		#define _inc_11 12
	#endif
	#ifndef _inc_12
		#define _inc_12 0
	#endif
	#ifndef _dec_0
		#define _dec_0 12
	#endif
	#define _dec_12 11

	/* logic */
	#define IS_12_OR_LESS(x) OR(IS_11_OR_LESS(x),EQUAL(12,x))

	/* looping */
	#define PROTO_FROM_12(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,12)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),12),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),12)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,12,do_macro(12,__VA_ARGS__))

#endif /* BASE-13 : USE_TRIDECIMAL_NUMBERS */


#if USE_BASE == 12 || defined(USE_DUODECIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_UNDECIMAL_NUMBERS
		#define USE_UNDECIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_11(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 11
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_10
		#define _inc_10 11
	#endif
	#ifndef _inc_11
		#define _inc_11 0
	#endif
	#ifndef _dec_0
		#define _dec_0 11
	#endif
	#define _dec_11 10

	/* logic */
	#define IS_11_OR_LESS(x) OR(IS_10_OR_LESS(x),EQUAL(11,x))

	/* looping */
	#define PROTO_FROM_11(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,11)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),11),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),11)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,11,do_macro(11,__VA_ARGS__))

#endif /* BASE-12 : USE_DUODECIMAL_NUMBERS */


#if USE_BASE == 11 || defined(USE_UNDECIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_DECIMAL_NUMBERS
		#define USE_DECIMAL_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_10(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 10
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_9
		#define _inc_9 10
	#endif
	#ifndef _inc_10
		#define _inc_10 0
	#endif
	#ifndef _dec_0
		#define _dec_0 10
	#endif
	#define _dec_10 9

	/* logic */
	#define IS_10_OR_LESS(x) OR(IS_9_OR_LESS(x),EQUAL(10,x))

	/* looping */
	#define PROTO_FROM_10(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,10)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),10),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),10)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,10,do_macro(10,__VA_ARGS__))

#endif /* BASE-11 : USE_UNDECIMAL_NUMBERS */


#if USE_BASE == 10 || defined(USE_DECIMAL_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_NONARY_NUMBERS
		#define USE_NONARY_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_9(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 9
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_8
		#define _inc_8 9
	#endif
	#ifndef _inc_9
		#define _inc_9 0
	#endif
	#ifndef _dec_0
		#define _dec_0 9
	#endif
	#define _dec_9 8

	/* logic */
	#define IS_9_OR_LESS(x) OR(IS_8_OR_LESS(x),EQUAL(9,x))

	/* looping */
	#define PROTO_FROM_9(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,9)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),9),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),9)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,9,do_macro(9,__VA_ARGS__))

#endif /* BASE-10 : USE_DECIMAL_NUMBERS */


#if USE_BASE == 9 || defined(USE_NONARY_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_OCTARY_NUMBERS
		#define USE_OCTARY_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_8(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 8
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_7
		#define _inc_7 8
	#endif
	#ifndef _inc_8
		#define _inc_8 0
	#endif
	#ifndef _dec_0
		#define _dec_0 8
	#endif
	#define _dec_8 7

	/* logic */
	#define IS_8_OR_LESS(x) OR(IS_7_OR_LESS(x),EQUAL(8,x))

	/* looping */
	#define PROTO_FROM_8(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,8)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),8),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),8)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,8,do_macro(8,__VA_ARGS__))

#endif /* BASE-9 : USE_NONARY_NUMBERS */


#if USE_BASE == 8 || defined(USE_OCTARY_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_SEPTARY_NUMBERS
		#define USE_SEPTARY_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_7(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 7
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_6
		#define _inc_6 7
	#endif
	#ifndef _inc_7
		#define _inc_7 0
	#endif
	#ifndef _dec_0
		#define _dec_0 7
	#endif
	#define _dec_7 6

	/* logic */
	#define IS_7_OR_LESS(x) OR(IS_6_OR_LESS(x),EQUAL(7,x))

	/* looping */
	#define PROTO_FROM_7(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,7)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),7),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),7)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,7,do_macro(7,__VA_ARGS__))

#endif /* BASE-8 : USE_OCTARY_NUMBERS */


#if USE_BASE == 7 || defined(USE_SEPTARY_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_SENARY_NUMBERS
		#define USE_SENARY_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_6(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 6
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_5
		#define _inc_5 6
	#endif
	#ifndef _inc_6
		#define _inc_6 0
	#endif
	#ifndef _dec_0
		#define _dec_0 6
	#endif
	#define _dec_6 5

	/* logic */
	#define IS_6_OR_LESS(x) OR(IS_5_OR_LESS(x),EQUAL(6,x))

	/* looping */
	#define PROTO_FROM_6(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,6)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),6),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),6)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,6,do_macro(6,__VA_ARGS__))

#endif /* BASE-7 : USE_SEPTARY_NUMBERS */


#if USE_BASE == 6 || defined(USE_SENARY_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_QUINARY_NUMBERS
		#define USE_QUINARY_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_5(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 5
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_4
		#define _inc_4 5
	#endif
	#ifndef _inc_5
		#define _inc_5 0
	#endif
	#ifndef _dec_0
		#define _dec_0 5
	#endif
	#define _dec_5 4

	/* logic */
	#define IS_5_OR_LESS(x) OR(IS_4_OR_LESS(x),EQUAL(5,x))

	/* looping */
	#define PROTO_FROM_5(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,5)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),5),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),5)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,5,do_macro(5,__VA_ARGS__))

#endif /* BASE-6 : USE_SENARY_NUMBERS */


#if USE_BASE == 5 || defined(USE_QUINARY_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_QUATARY_NUMBERS
		#define USE_QUATARY_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_4(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 4
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_3
		#define _inc_3 4
	#endif
	#ifndef _inc_4
		#define _inc_4 0
	#endif
	#ifndef _dec_0
		#define _dec_0 4
	#endif
	#define _dec_4 3

	/* logic */
	#define IS_4_OR_LESS(x) OR(IS_3_OR_LESS(x),EQUAL(4,x))

	/* looping */
	#define PROTO_FROM_4(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,4)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),4),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),4)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,4,do_macro(4,__VA_ARGS__))

#endif /* BASE-5 : USE_QUINARY_NUMBERS */


#if USE_BASE == 4 || defined(USE_QUATARY_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_TERNARY_NUMBERS
		#define USE_TERNARY_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_3(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 3
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_2
		#define _inc_2 3
	#endif
	#ifndef _inc_3
		#define _inc_3 0
	#endif
	#ifndef _dec_0
		#define _dec_0 3
	#endif
	#define _dec_3 2

	/* logic */
	#define IS_3_OR_LESS(x) OR(IS_2_OR_LESS(x),EQUAL(3,x))

	/* looping */
	#define PROTO_FROM_3(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,3)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),3),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),3)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,3,do_macro(3,__VA_ARGS__))

#endif /* BASE-4 : USE_QUATARY_NUMBERS */


#if USE_BASE == 3 || defined(USE_TERNARY_NUMBERS)
	/* include the numbers from before */
	#ifndef USE_BINARY_NUMBERS
		#define USE_BINARY_NUMBERS
	#endif

	/* definition of new number (counting started at zero) */
	#define COMPARE_2(x) x
	#ifndef LARGEST_NUMERAL
		#define LARGEST_NUMERAL 2
	#endif

	/* fix and add to the increment,decrement abilities */
	#ifndef _inc_1
		#define _inc_1 2
	#endif
	#ifndef _inc_2
		#define _inc_2 0
	#endif
	#ifndef _dec_0
		#define _dec_0 2
	#endif
	#define _dec_2 1

	/* logic */
	#define IS_2_OR_LESS(x) OR(IS_1_OR_LESS(x),EQUAL(2,x))

	/* looping */
	#define PROTO_FROM_2(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,2)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),2),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),2)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,2,do_macro(2,__VA_ARGS__))

#endif /* BASE-3 : USE_TERNARY_NUMBERS */




#if USE_BASE == 2 || defined(USE_BINARY_NUMBERS)
	/* definitions of new numbers (counting starts at zero) */
	#define COMPARE_0(x) x
	#define SMALLEST_NUMERAL 0
	#define COMPARE_1(x) x
	#ifndef LARGEST_NUMERAL
		#define  LARGEST_NUMERAL 1
	#endif
	/* add the increment,decrement abilities */
	#ifndef _inc_0
		#define _inc_0 1
	#endif
	#ifndef _inc_1
		#define _inc_1 0
	#endif
	#ifndef _dec_0
		#define _dec_0 1
	#endif
	#define _dec_1 0
	/* logic */
	#define IS_0_OR_LESS(x) EQUAL(0,x)
	#define IS_1_OR_LESS(x) OR(EQUAL(1,x),EQUAL(0,x))

	/* looping */
	#define PROTO_FROM_0(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,0)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),0),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),0)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,0,do_macro(0,__VA_ARGS__))

	#define PROTO_FROM_1(do_macro,breakout_macro,__up__,to,n,...) \
		IF(NOT_EQUAL(to,1)) \
		( \
			IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),1),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \
				(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),1)),breakout_macro) \
			,breakout_macro \
		)(do_macro,breakout_macro,__up__,to,1,do_macro(1,__VA_ARGS__))
#endif /*USE_BINARY_NUMBERS*/

#endif /*CNLPPL_NUMBER_BASES_H*/
