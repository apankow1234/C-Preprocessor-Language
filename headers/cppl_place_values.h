/*
	Andrew Pankow
	Language c
*/
#ifndef CNLPPL_PLACE_VALUES_H
#define CNLPPL_PLACE_VALUES_H
#define USE_NUM_PLACE_VALUES 2
/*
	C's Preprocessor Language - Place Values.
*/

/*
	COUNTER
	I am recreating the POP-COUNT() macro with way more flexibility and control.

	This will be the section dedicated to the the sequence/counter items.
	Although, this seems way more long winded than necessary, I absolutely wanted
	the closest thing to dynamic as I could get; so we'll settle on "highly
	extensible".

	Goals:
		* to be able to count the number of arguments passed
		* make apparent loops since recursion isn't a thing for the gcc compiler
		* check for empty arguments
		* ... etc?
*/
#define CSN(...) CLEAN(_build,LARGEST_USABLE_PV)(RUN(__VA_ARGS__))
#define UP(...) CYCLE(1,__VA_ARGS__) // for testing, will implement similar
#define DN(...) CYCLE(0,__VA_ARGS__) // for testing, will implement similar


#define CSN_NOT_EQUAL(...) CLEAN(_ne,LARGEST_USABLE_PV)(__VA_ARGS__)
#define CSN_EQUAL(...) CLEAN(_eq,LARGEST_USABLE_PV)(__VA_ARGS__)
#define CSN_LESS_THAN_OR_EQUAL_TO(...) CLEAN(_le,LARGEST_USABLE_PV)(__VA_ARGS__)
#define CSN_LESS_THAN(...) CLEAN(_lt,LARGEST_USABLE_PV)(__VA_ARGS__)
#define CSN_GREATER_THAN_OR_EQUAL_TO(...) CLEAN(_ge,LARGEST_USABLE_PV)(__VA_ARGS__)
#define CSN_GREATER_THAN(...) CLEAN(_gt,LARGEST_USABLE_PV)(__VA_ARGS__)

/*Run against all place-values*/
#define nEq(...) CLEAN(pvEq,LARGEST_USABLE_PV)(__VA_ARGS__)
#define nNEq(...) NOT(CLEAN(pvEq,LARGEST_USABLE_PV)(__VA_ARGS__))
#define nLT(...) CLEAN(pvLT,LARGEST_USABLE_PV)(__VA_ARGS__)
#define nLE(...)  OR( \
		CLEAN(pvLT,LARGEST_USABLE_PV)(__VA_ARGS__) \
		,CLEAN(pvEq,LARGEST_USABLE_PV)(__VA_ARGS__) \
	)
#define nGT(...) CLEAN(pvGT,LARGEST_USABLE_PV)(__VA_ARGS__)
#define nGE(...) OR( \
		CLEAN(pvGT,LARGEST_USABLE_PV)(__VA_ARGS__) \
		,CLEAN(pvEq,LARGEST_USABLE_PV)(__VA_ARGS__) \
	)


#define CSN_BETWEEN(x,min,max) CLEAN(_bt,LARGEST_USABLE_PV)(x,min,max)
#define CSN_IN_BETWEEN(x,max,min) CLEAN(_ib,LARGEST_USABLE_PV)(x,min,max)


/*
	PLACE-VALUE DEFINITIONS
	Sequences are going to be forced Little-endian internally for macro simplicity.
	This means that the macros will see the numbers in reverse (1024 will be 4201).

	Each defined place-value will shift the sequence to the right.
	Each undefined place-value will make it so that if they are used, they
	will simply be ignored...
	If a place-value is defined but unused, it is also ignored.

	Example... (7,8,0,1) if __1000s__ are defined ([x,x,x,x]) will be 1087
	but if only up to the __10s__ are defined ([x,x],x,x), (7,8,0,1) will be 87

	Define or retract as many as you need. Hopefully the process is painless.
*/

/*
	This is the macro that selects from the _inc_# and _dec_# lists to obtain
	the next appropriate numeral. pv is the defined place-value's name and
	__up__ represents the direction of counting, 1 for up and 0 for down.
*/
#define _change_numbers(pv,__up__,...) \
	CLEAN(CLEAN(CLEAN(_,IF(__up__)(inc,dec)),_),CONCAT(_get,pv)(__VA_ARGS__))

/*
	Check if the number is 9 or 0[zero] and if so return 1[true].
	This is for use with multiple place-values (pvs) to keep track of the lower
	pv and, depending on the direction (__up__ variable) of counting, will allow
	us to cycle the higher pv up/down.
*/
#define WILL_ROLLOVER(pv,__up__,...) \
	IF(IS_DEFINED(,pv)) \
	( \
		EQUAL(CLEAN(_get,pv)(__VA_ARGS__),CLEAN(IF(__up__)(9,0))) \
		, 0\
	)
/*
	Each place-value (pv) will define it's previous and next pvs.
	If a pv doesn't have a previous pv (like the ones' place), let it always
	increment. If the pv does have a preveious pv, check that previous pv for a
	9 or 0 and then do the appropriate roll given the direction (__up__ variable)
*/
#define IS_SMALLEST(pv) EQUAL(CONCAT(_pv_dec,pv),0)
#define IS_LARGEST(pv) EQUAL(CONCAT(_pv_inc,pv),0)
#define _cycle(pv,__up__,...) \
	IF(AND(IS_DEFINED(,pv),NOT(IS_SMALLEST(pv)))) \
	( \
		IF(CLEAN(_will_cycle,pv)(__up__,__VA_ARGS__)) \
		( \
			_change_numbers(pv,__up__,__VA_ARGS__) \
			,CONCAT(_get,pv)(__VA_ARGS__) \
		)\
		, _change_numbers(pv,__up__,__VA_ARGS__)\
	)
#define _cycle_this(pv,__up__,...) \
	IF(IS_BLANK(RUN(CONCAT(_get,pv)(__VA_ARGS__)))) \
	( \
		CLEAN(_cycle,CONCAT(_pv_dec,pv))(__up__,__VA_ARGS__) \
		,RUN(CLEAN(_cycle,CONCAT(_pv_dec,pv))(__up__,__VA_ARGS__),_cycle(pv,__up__,__VA_ARGS__)) \
	)
/*
	_cycle#s(__up__,...) is the macro that includes the previous place-values in the
	incrementing or decrementing.

	CYCLE(__up__,...) is the API-type call that allows for the scalability of this into
	higher place-values.
*/
#define CYCLE(__up__,...) CLEAN(_cycle,LARGEST_USABLE_PV)(__up__,__VA_ARGS__)
/*
	RASTERIZE_NUMBER(...) is the API-type call that calls the highest defined place-
	value's _raster_#s(...) macro and thus printing out a Big-endian (standard) number
	for final use in c or in a counter-offset.
*/
//#define POP(...) CLEAN(_pv_pop,LARGEST_USABLE_PV)
#define RASTERIZE_NUMBER(...) CLEAN(_raster,LARGEST_USABLE_PV)(__VA_ARGS__)

#define _primary_num(...) CSN(__VA_ARGS__)
#define _has_secondary_num(...) IF(EMPTY_SEQUENCE(CLEAN(_pv_pop,LARGEST_USABLE_PV)(__VA_ARGS__)))(0,1)
#define _secondary_num(...) CSN(CLEAN(_pv_pop,LARGEST_USABLE_PV)(__VA_ARGS__))
#define _tertiary_num(...) CSN(CLEAN(_pv_dpop,LARGEST_USABLE_PV)(__VA_ARGS__))


#define _ne(pv,...) NOT_EQUAL(CLEAN(_cmp,pv)(__VA_ARGS__),CLEAN(_cmp,pv)(__VA_ARGS__))
#define _eq(pv,...) EQUAL(CLEAN(_cmp,pv)(__VA_ARGS__),CLEAN(_cmp,pv)(__VA_ARGS__))
#define _lt(pv,...) X_LESS_THAN(CLEAN(_cmp,pv)(__VA_ARGS__),CLEAN(_cmp,pv)(__VA_ARGS__))
#define _le(pv,...) X_LESS_THAN_OR_EQUAL_TO(CLEAN(_cmp,pv)(__VA_ARGS__),CLEAN(_cmp,pv)(__VA_ARGS__))
#define _gt(pv,...) X_GREATER_THAN(CLEAN(_cmp,pv)(__VA_ARGS__),CLEAN(_cmp,pv)(__VA_ARGS__))
#define _ge(pv,...) X_GREATER_THAN_OR_EQUAL_TO(CLEAN(_cmp,pv)(__VA_ARGS__),CLEAN(_cmp,pv)(__VA_ARGS__))

/*Per Place-Value*/
#define pvEq(pv,...) \
	EQUAL(CLEAN(_cmp,pv)(_primary_num(__VA_ARGS__)) \
			,CLEAN(_cmp,pv)(_secondary_num(__VA_ARGS__)))
#define pvLT(pv,...) \
	X_LESS_THAN(CLEAN(_cmp,pv)(_primary_num(__VA_ARGS__)) \
			,CLEAN(_cmp,pv)(_secondary_num(__VA_ARGS__)))
#define pvGT(pv,...) \
	X_GREATER_THAN(CLEAN(_cmp,pv)(_primary_num(__VA_ARGS__)) \
			,CLEAN(_cmp,pv)(_secondary_num(__VA_ARGS__)))

/*for between and inbetween min and max order doesn't matter*/
#define _bt(pv,...) X_BETWEEN(CLEAN(_cmp,pv)(__VA_ARGS__),CLEAN(_cmp,pv)(__VA_ARGS__),CLEAN(_cmp,pv)(__VA_ARGS__))
#define _ib(pv,...) X_IN_BETWEEN(CLEAN(_cmp,pv)(__VA_ARGS__),CLEAN(_cmp,pv)(__VA_ARGS__),CLEAN(_cmp,pv)(__VA_ARGS__))

/*
	PLACE-VALUE: 1's

	This is a baseline place-value for what can be appropriate in the preprocessor without
	taking years or something to implement a fractional pv system... So, the ones' it is.
*/
#if 1
	/*header*/
	#define _1s(x) x /*define this place-value*/
	#define _pv_dec_1s 0 /*next lower place-value | zero means none*/
	#define _pv_inc_1s 0 /*next higher place-value | zero means none*/
	#define _pv_pop_1s(x,...) __VA_ARGS__ /*give me all place-values beyond this place*/
	#define _pv_dpop_1s(x,y,...) __VA_ARGS__ /*give me all place-values beyond double of this place*/
	#define _pv_1s() CSN(1) /*give me the order-n of this place-value*/
	#define SMALLEST_USABLE_PV _1s
	#define LARGEST_USABLE_PV _1s

	/*body*/
	/* constructor */
	#define _build_1s(...) IF(IS_BLANK(_get_1s(__VA_ARGS__)))(0,_get_1s(__VA_ARGS__))

	#define _rbuild_1s(...) IF(IS_BLANK(_get_1s(__VA_ARGS__)))(END,_get_1s)(__VA_ARGS__)

	/* cycling through numbers/rolling over */
//	#define _get_clean_1s(x,...) x /*Little-endian sequences*/
//	#define _cmp_1s(x,...) POP(x,) /*Little-endian sequences*/
	#define _cmp_1s(x,...) POP(x,)
	#define _get_1s(x,...) x /*Little-endian sequences*/
	#define _after_1s(x,...) __VA_ARGS__ /*Little-endian sequences*/
	#define _raster_1s(...) _get_1s(__VA_ARGS__) // Big-endian final concatenation
	#define _cycle_1s(__up__,...) _cycle(_1s,__up__,__VA_ARGS__)
	#define _will_cycle_1s(__up__,...) WILL_ROLLOVER(_1s,__up__,__VA_ARGS__)

	/* comparison - for between and in-between, min and max can be switched around */
	#define _ne_1s(...) _ne(_1s,__VA_ARGS__)
	#define _eq_1s(...) _eq(_1s,__VA_ARGS__)
	#define _le_1s(...) _le(_1s,__VA_ARGS__)
	#define _lt_1s(...) _lt(_1s,__VA_ARGS__)
	#define _ge_1s(...) _ge(_1s,__VA_ARGS__)
	#define _gt_1s(...) _gt(_1s,__VA_ARGS__)

	#define pvEq_1s(...) pvEq(_1s,__VA_ARGS__)
	#define pvNEq_1s(...) pvNEq(_1s,__VA_ARGS__)
	#define pvLT_1s(...) pvLT(_1s,__VA_ARGS__)
	#define pvLE_1s(...) pvLE(_1s,__VA_ARGS__)
	#define pvGT_1s(...) pvGT(_1s,__VA_ARGS__)
	#define pvGE_1s(...) pvGE(_1s,__VA_ARGS__)

	#define _bt_1s(...) _bt(_1s,__VA_ARGS__) /*min and max order doesn't matter*/
	#define _ib_1s(...) _ib(_1s,__VA_ARGS__) /*min and max order doesn't matter*/

#endif /* PLACE-VALUE: 1's (the 1st place-value) */

/* PLACE-VALUE: 10's (the 2nd place-value) */
#if USE_NUM_PLACE_VALUES >= 2
	/*header*/
	#define _10s(x) x /*define this place-value*/
	#undef LARGEST_USABLE_PV
		#define LARGEST_USABLE_PV _10s
	#undef _pv_inc_1s
		#define _pv_inc_1s _10s /*reset next lower place-value to this place-value*/
	#define _pv_dec_10s _1s /*next lower place-value | zero means none*/
	#define _pv_inc_10s 0 /*next higher place-value | zero means none*/
	#define _pv_pop_10s(x,...) CLEAN(_pv_pop,_pv_dec_10s)(__VA_ARGS__) /*give me all place-values beyond this place*/
	#define _pv_dpop_10s(x,y,...) CLEAN(_pv_dpop,_pv_dec_10s)(__VA_ARGS__) /*give me all place-values beyond double of this place*/
	#define _pv_10s() UP(CLEAN(_pv,_pv_dec_10s)()) /*give me the order-n of this place-value*/

	/*body*/
	/* constructor */
	#define _build_10s(...) \
		RUN(RUN(CLEAN(_build,_pv_dec_10s)(__VA_ARGS__)) \
		,RUN(IF(IS_BLANK(_get_10s(__VA_ARGS__)))(0,_get_10s(__VA_ARGS__))))

	#define _rbuild_10s(...) \
		IF(IS_BLANK(_get_10s(__VA_ARGS__)))(0,_get_10s(__VA_ARGS__)) \
		,CLEAN(_rbuild,_pv_dec_10s)(__VA_ARGS__),

	/* cycling through numbers/rolling over */
//	#define _get_clean_10s(x,...) _cmp_1s(__VA_ARGS__)
//	#define _cmp_10s(x,...) _get_clean_10s(x,)
	#define _cmp_10s_clean(x,...) _cmp_1s(__VA_ARGS__)
	#define _cmp_10s(x,...) _cmp_10s_clean(x,)
	#define _get_10s(x,...) CLEAN(_get,_pv_dec_10s)(__VA_ARGS__) /*Little-endian sequences*/
	#define _after_10s(x,...) CLEAN(_after,_pv_dec_10s)(__VA_ARGS__) /*Little-endian sequences*/
	#define _raster_10s(...) /*Big-endian final concatenation*/ \
		IF(IS_BLANK(_get_10s(__VA_ARGS__))) \
		( \
			CLEAN(_raster,_pv_dec_10s)(__VA_ARGS__) \
			,CLEAN(_get_10s(__VA_ARGS__),CLEAN(_raster,_pv_dec_10s)(__VA_ARGS__)) \
		)
	/*checks to make sure that the place-value was even used first and then cycles*/
	#define _cycle_10s(__up__,...) \
		IF(IS_BLANK(_get_10s(__VA_ARGS__))) \
		( \
			CLEAN(_cycle,_pv_dec_10s)(__up__,__VA_ARGS__) \
			,RUN(CLEAN(_cycle,_pv_dec_10s)(__up__,__VA_ARGS__),_cycle(_10s,__up__,__VA_ARGS__)) \
		)
	/*checks to see if this place-value will cycle up or down*/
	#define _will_cycle_10s(__up__,...) \
		AND( \
			CLEAN(_will_cycle,_pv_dec_10s)(__up__,__VA_ARGS__) \
			,WILL_ROLLOVER(_pv_dec_10s,__up__,__VA_ARGS__) \
		)

	/* comparison - for between and in-between, min and max can be switched around */
	#define _ne_10s(...) AND(CLEAN(_ne,_pv_dec_10s)(__VA_ARGS__),_ne(_10s,__VA_ARGS__))
	#define _eq_10s(...) AND(CLEAN(_eq,_pv_dec_10s)(__VA_ARGS__),_eq(_10s,__VA_ARGS__))
	#define _le_10s(...) IF(_le(_10s,__VA_ARGS__))(1,CLEAN(_le,_pv_dec_10s)(__VA_ARGS__))
	#define _lt_10s(...) IF(_lt(_10s,__VA_ARGS__))(1,CLEAN(_lt,_pv_dec_10s)(__VA_ARGS__))
	#define _ge_10s(...) IF(_ge(_10s,__VA_ARGS__))(1,CLEAN(_ge,_pv_dec_10s)(__VA_ARGS__))
	#define _gt_10s(...) IF(_gt(_10s,__VA_ARGS__))(1,CLEAN(_gt,_pv_dec_10s)(__VA_ARGS__))

	#define pvEq_10s(...) AND(pvEq(_10s,__VA_ARGS__),pvEq_1s(__VA_ARGS__))
	#define pvNEq_10s(...) AND(pvNEq(_10s,__VA_ARGS__),pvNEq_1s(__VA_ARGS__))
	#define pvLT_10s(...) IF(pvLT(_10s,__VA_ARGS__))(1,pvLT_1s(__VA_ARGS__))
	#define pvLE_10s(...) IF(pvLE(_10s,__VA_ARGS__))(1,pvLE_1s(__VA_ARGS__))
	#define pvGT_10s(...) IF(pvGT(_10s,__VA_ARGS__))(1,pvGT_1s(__VA_ARGS__))
	#define pvGE_10s(...) IF(pvGE(_10s,__VA_ARGS__))(1,pvGE_1s(__VA_ARGS__))

	#define _bt_10s(...) IF(_bt(_10s,__VA_ARGS__))(1,CLEAN(_bt,_pv_dec_10s)(__VA_ARGS__))
	#define _ib_10s(...) IF(_ib(_10s,__VA_ARGS__))(1,CLEAN(_ib,_pv_dec_10s)(__VA_ARGS__))
#endif /* PLACE-VALUE: 10's (the 2nd place-value) */





/*RANGE(from,to) takes lists of place values separated by commas.
	example. RANGE((__1000s__,__100s__,__10s__,__1s__),(__1000s__,__100s__,__10s__,__1s__))
*/
//#define RANGE(from,to)
#define SEQUENCE_UP_TO(from,to)
#define SEQUENCE_UP_1s(from,to) PROTO_UP(from,to)
#define CLEAN_SEQ(x,...) IF(IS_FIRST_EMPTY(__VA_ARGS__))(DUMP_FIRST_ARG,RUN)(__VA_ARGS__,x)


#define CLEANSE(...) IF(IS_FIRST_EMPTY(__VA_ARGS__))(DUMP_FIRST_ARG,RUN)(__VA_ARGS__)


#define DISP_SEQ_TO(do_macro,breakout_macro,__up__,to,n,...) IF(IS_FIRST_EMPTY(__VA_ARGS__))(DUMP_FIRST_ARG,RUN)(__VA_ARGS__)

#define dumb_counter(x,...) IF(IS_FIRST_EMPTY(__VA_ARGS__))(CSN(),UP(_primary_num(__VA_ARGS__)))
//#define disp_count(do_macro,breakout_macro,__up__,to,n,...) RUN(dumb_counter(n,__VA_ARGS__))// inclusive count
#define disp_count(do_macro,breakout_macro,__up__,to,n,...) __VA_ARGS__ //regular count





#define __lp_pos(...) _primary_num(__VA_ARGS__)
#define __to_val(...) _secondary_num(__VA_ARGS__)
#define __fm_val(...) _tertiary_num(__VA_ARGS__)

/*
	do-macro                                    __do__
	break-out-macro                             __bk__

	loop-position-CSN (Comma Separated Number)  __lp_pos(__VA_ARGS__)

	to-value-CSN                                __to_val(__VA_ARGS__)
	from-value-CSN                              __fm_val(__VA_ARGS__)
*/

#define from_0(__do__,__bk__,pv,...) from_1(__do__,__bk__,pv,mfrom_0(__do__,__bk__,pv##0,__VA_ARGS__))
#define from_1(__do__,__bk__,pv,...) from_2(__do__,__bk__,pv,mfrom_0(__do__,__bk__,pv##1,__VA_ARGS__))
#define from_2(__do__,__bk__,pv,...) from_3(__do__,__bk__,pv,mfrom_0(__do__,__bk__,pv##2,__VA_ARGS__))
#define from_3(__do__,__bk__,pv,...) from_4(__do__,__bk__,pv,mfrom_0(__do__,__bk__,pv##3,__VA_ARGS__))
#define from_4(__do__,__bk__,pv,...) from_5(__do__,__bk__,pv,mfrom_0(__do__,__bk__,pv##4,__VA_ARGS__))
#define from_5(__do__,__bk__,pv,...) from_6(__do__,__bk__,pv,mfrom_0(__do__,__bk__,pv##5,__VA_ARGS__))
#define from_6(__do__,__bk__,pv,...) from_7(__do__,__bk__,pv,mfrom_0(__do__,__bk__,pv##6,__VA_ARGS__))
#define from_7(__do__,__bk__,pv,...) from_8(__do__,__bk__,pv,mfrom_0(__do__,__bk__,pv##7,__VA_ARGS__))
#define from_8(__do__,__bk__,pv,...) from_9(__do__,__bk__,pv,mfrom_0(__do__,__bk__,pv##8,__VA_ARGS__))
#define from_9(__do__,__bk__,pv,...) mfrom_0(__do__,__bk__,pv##9,__VA_ARGS__)

/*
	do-macro                                    __do__
	break-out-macro                             __bk__

	loop-position-CSN (Comma Separated Number)  __lp_pos(__VA_ARGS__)

	to-value-CSN                                __to_val(__VA_ARGS__)
	from-value-CSN                              __fm_val(__VA_ARGS__)
*/
#define mfrom_0(__do__,__bk__,pv,...) mfrom_1(__do__,__bk__,pv,nfrom_0(__do__,__bk__,pv##0,__VA_ARGS__))
#define mfrom_1(__do__,__bk__,pv,...) mfrom_2(__do__,__bk__,pv,nfrom_0(__do__,__bk__,pv##1,__VA_ARGS__))
#define mfrom_2(__do__,__bk__,pv,...) mfrom_3(__do__,__bk__,pv,nfrom_0(__do__,__bk__,pv##2,__VA_ARGS__))
#define mfrom_3(__do__,__bk__,pv,...) mfrom_4(__do__,__bk__,pv,nfrom_0(__do__,__bk__,pv##3,__VA_ARGS__))
#define mfrom_4(__do__,__bk__,pv,...) mfrom_5(__do__,__bk__,pv,nfrom_0(__do__,__bk__,pv##4,__VA_ARGS__))
#define mfrom_5(__do__,__bk__,pv,...) mfrom_6(__do__,__bk__,pv,nfrom_0(__do__,__bk__,pv##5,__VA_ARGS__))
#define mfrom_6(__do__,__bk__,pv,...) mfrom_7(__do__,__bk__,pv,nfrom_0(__do__,__bk__,pv##6,__VA_ARGS__))
#define mfrom_7(__do__,__bk__,pv,...) mfrom_8(__do__,__bk__,pv,nfrom_0(__do__,__bk__,pv##7,__VA_ARGS__))
#define mfrom_8(__do__,__bk__,pv,...) mfrom_9(__do__,__bk__,pv,nfrom_0(__do__,__bk__,pv##8,__VA_ARGS__))
#define mfrom_9(__do__,__bk__,pv,...) nfrom_0(__do__,__bk__,pv##9,__VA_ARGS__)


/*
	do-macro                                    __do__
	break-out-macro                             __bk__

	loop-position-CSN (Comma Separated Number)  __lp_pos(__VA_ARGS__)

	to-value-CSN                                __to_val(__VA_ARGS__)
	from-value-CSN                              __fm_val(__VA_ARGS__)
*/

#define qRas(...) RASTERIZE_NUMBER(__lp_pos(__VA_ARGS__))
#define exp(...) UP(__lp_pos(__VA_ARGS__))

#define to_isnt_from(...) CSN_NOT_EQUAL(__to_val(__VA_ARGS__),__fm_val(__VA_ARGS__))

#define CLEAN_ARGS(__do__,__bk__,...)

#define nfrom_0(__do__,__bk__,...) \
	IF(to_isnt_from(__VA_ARGS__)) \
	(                                /*IF to-value AND from-value ARE NOT EQUAL, THEN*/ \
		nfrom_1(                     /*PASS ONTO THE NEXT*/ \
			__do__                     /*do-macro*/ \
			,__bk__                    /*break-out-macro*/ \
			,UP(__lp_pos(__VA_ARGS__)) /*loop-position-CSN*/ \
			,__to_val(__VA_ARGS__)     /*to-value-CSN*/ \
			,__fm_val(__VA_ARGS__)     /*from-value-CSN*/ \
			,__do__(__VA_ARGS__)       /*do-macro actually doing something*/ \
		)                            /*OR ELSE*/ \
		,__bk__(__VA_ARGS__)           /*break-out-macro does something*/ \
	)

#define nfrom_1(__do__,__bk__,...) \
	IF(to_isnt_from(__VA_ARGS__)) \
	( \
		nfrom_2(__do__,__bk__ ,UP(__lp_pos(__VA_ARGS__)) \
			,__to_val(__VA_ARGS__),__fm_val(__VA_ARGS__)) \
		,__bk__(__VA_ARGS__) \
	)

#define nfrom_2(__do__,__bk__,...) \
	IF(to_isnt_from(__VA_ARGS__)) \
	( \
		nfrom_3(__do__,__bk__ ,UP(__lp_pos(__VA_ARGS__)) \
			,__to_val(__VA_ARGS__),__fm_val(__VA_ARGS__)) \
		,__bk__(__VA_ARGS__) \
	)
#define nfrom_3(__do__,__bk__,...) \
	IF(to_isnt_from(__VA_ARGS__)) \
	( \
		nfrom_4(__do__,__bk__ ,UP(__lp_pos(__VA_ARGS__)) \
			,__to_val(__VA_ARGS__),__fm_val(__VA_ARGS__)) \
		,__bk__(__VA_ARGS__) \
	)

#define nfrom_4(__do__,__bk__,...) \
	IF(to_isnt_from(__VA_ARGS__)) \
	( \
		nfrom_5(__do__,__bk__ ,UP(__lp_pos(__VA_ARGS__)) \
			,__to_val(__VA_ARGS__),__fm_val(__VA_ARGS__)) \
		,__bk__(__VA_ARGS__) \
	)

#define nfrom_5(__do__,__bk__,...) \
	IF(to_isnt_from(__VA_ARGS__)) \
	( \
		nfrom_6(__do__,__bk__ ,UP(__lp_pos(__VA_ARGS__)) \
			,__to_val(__VA_ARGS__),__fm_val(__VA_ARGS__)) \
		,__bk__(__VA_ARGS__) \
	)

#define nfrom_6(__do__,__bk__,...) \
	IF(to_isnt_from(__VA_ARGS__)) \
	( \
		nfrom_7(__do__,__bk__ ,UP(__lp_pos(__VA_ARGS__)) \
			,__to_val(__VA_ARGS__),__fm_val(__VA_ARGS__)) \
		,__bk__(__VA_ARGS__) \
	)

#define nfrom_7(__do__,__bk__,...) \
	IF(to_isnt_from(__VA_ARGS__)) \
	( \
		nfrom_8(__do__,__bk__ ,UP(__lp_pos(__VA_ARGS__)) \
			,__to_val(__VA_ARGS__),__fm_val(__VA_ARGS__)) \
		,__bk__(__VA_ARGS__) \
	)

#define nfrom_8(__do__,__bk__,...) \
	IF(to_isnt_from(__VA_ARGS__)) \
	( \
		nfrom_9(__do__,__bk__ ,UP(__lp_pos(__VA_ARGS__)) \
			,__to_val(__VA_ARGS__),__fm_val(__VA_ARGS__)) \
		,__bk__(__VA_ARGS__) \
	)

#define nfrom_9(__do__,__bk__,...) \
	IF(to_isnt_from(__VA_ARGS__)) \
	( \
		__do__(__VA_ARGS__) \
		,__bk__(__VA_ARGS__) \
	)






#if USE_NUM_PLACE_VALUES > 2
	#include "cppl_extended_places.h"
#endif

#endif /*CNLPPL_PLACE_VALUES_H*/
