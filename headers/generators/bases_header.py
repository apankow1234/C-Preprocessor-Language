# Andrew Pankow
# Language python
# C's Preprocessor Language - Numbers for the bases

class NumberDefinition:
	#Greek root-name pieces
	relations = ["un","bin","tern","quatern","quin","sen","sept","oct","non"]
	least_significant = ["","un","duo","tri","tetra","penta","hexa","hepta","octo","non"]
	bases = ["","dec","vige","trige","quadrage","guinquage","sexage","spetage","octage","nonage"]
	#comments
	includesComment = "/* include the numbers from before */"
	definitionsComment = "/* definition of new number (counting started at zero) */"
	incrementalsComment = "/* fix and add to the increment,decrement abilities */"
	logicComment = "/* logic */"
	loopingComment = "/* looping */"

	def __init__(self,number,numPlaceValues=1,startingIndent=0):
		self.n = number
		self.numPV = numPlaceValues
		self.greek = NumberDefinition.getGreekName(self.n) #just for fun
		self.s = "" #the whole string that gets returned when you print this object
		self.i = startingIndent
		self.i1 = ((1+startingIndent)*"\t")
		self.i2 = ((2+startingIndent)*"\t")
		i = (startingIndent*"\t")
		self.open = i+"#if USE_BASE == "+str(self.n)+" || defined("+NumberDefinition.useNum(self.n)+")\n"
		self.close = i+"#endif /* BASE-"+str(self.n)+" : "+NumberDefinition.useNum(self.n)+" */\n\n"
		
	@staticmethod
	def getGreekName(num) :
		greekName = ""
		if num < 10:
			greekName = NumberDefinition.relations[num-1]
			greekName += "ary"
		elif num >= 10 and num < 100 :
			greekName = NumberDefinition.least_significant[num%10]
			greekName += NumberDefinition.bases[(num-num%10)/10]
			greekName += ("s" if NumberDefinition.bases[(num-num%10)/10].endswith("e") else "")
			greekName += "imal"
		return greekName

	@staticmethod
	def useNum(num) :
		outString = "_".join(["use",NumberDefinition.getGreekName(num),"numbers"]).upper()
		return outString
	
	def defineNumber(self) :
		self.s = self.open
		self.s += self.addLowerNumbers()
		self.s += self.addDefinitions()
		self.s += self.addIncSection()
		self.s += self.addLogic()
		self.s += self.addLooping()
		self.s += self.close

	def addLowerNumbers(self):
		outString = self.i1+NumberDefinition.includesComment+"\n"
		outString += self.i1+"#ifndef "+NumberDefinition.useNum(self.n-1)+"\n"
		outString += self.i2+"#define "+NumberDefinition.useNum(self.n-1)+"\n"
		outString += self.i1+"#endif\n"
		outString += "\n"
		return outString

	def addDefinitions(self):
		outString = self.i1+NumberDefinition.definitionsComment+"\n"
		outString += self.i1+"#define COMPARE_"+str(self.n-1)+"(x) x\n"
		outString += self.i1+"#ifndef LARGEST_NUMERAL\n"
		outString += self.i2+"#define LARGEST_NUMERAL "+str(self.n-1)+"\n"
		outString += self.i1+"#endif\n"
		outString += "\n"
		return outString
	
	def addIncSection(self):
		outString = self.i1+NumberDefinition.incrementalsComment+"\n"
		outString += self.fixPreviousIncrement()
		outString += self.addIncrement()
		outString += self.fixPreviousDecrement()
		outString += self.addDecrement()
		outString += "\n"
		return outString
	
	def addLogic(self):
		outString = self.i1+NumberDefinition.logicComment+"\n"
		outString += self.i1+"#define IS_"+str(self.n-1)+"_OR_LESS(x) OR(IS_"
		outString += str(self.n-2)+"_OR_LESS(x),EQUAL("+str(self.n-1)+",x))\n"
		outString += "\n"
		return outString
	
	def addLooping(self):
		outString = self.i1+NumberDefinition.loopingComment+"\n"
		outString += self.buildProtoFrom()
		outString += "\n"
		return outString

	def makeNumberDef(self) :
		outString = "COMPARE_"+str(self.n)+"(x) x \n"
		return outString

	def addIncrement(self):
		outString = self.i1+"#ifndef _inc_"+str(self.n-1)+"\n"
		outString += self.i2+"#define _inc_"+str(self.n-1)+" 0\n"
		outString += self.i1+"#endif\n"
		return outString

	def fixPreviousIncrement(self):
		outString = self.i1+"#ifndef _inc_"+str(self.n-2)+"\n"
		outString += self.i2+"#define _inc_"+str(self.n-2)+" "+str(self.n-1)+"\n"
		outString += self.i1+"#endif\n"
		return outString

	def addDecrement(self):
		outString = self.i1+"#define _dec_"+str(self.n-1)+" "+str(self.n-2)+"\n"
		return outString

	def fixPreviousDecrement(self):
		outString = self.i1+"#ifndef _dec_0\n"
		outString += self.i2+"#define _dec_0 "+str(self.n-1)+"\n"
		outString += self.i1+"#endif\n"
		return outString

	def buildProtoFrom(self):
		i3 = ((3+self.i)*"\t")
		i4 = ((4+self.i)*"\t")
		outString = self.i1+"#define PROTO_FROM_"+str(self.n-1)+"(do_macro,breakout_macro,__up__,to,n,...) \\\n"
		outString += self.i2+"IF(NOT_EQUAL(to,"+str(self.n-1)+")) \\\n"
		outString += self.i2+"( \\\n"
		outString += i3+"IF(NOT_EQUAL(CLEAN(IF(__up__)(_inc_,_dec_),"
		outString += str(self.n-1)+"),CLEAN(IF(__up__)(SMALLEST,LARGEST),_NUMERAL))) \\\n"
		outString += i4+"(CLEAN(PROTO_FROM_,CLEAN(IF(__up__)(_inc_,_dec_),"+str(self.n-1)+")),breakout_macro) \\\n"
		outString += i3+",breakout_macro \\\n"
		outString += self.i2+")(do_macro,breakout_macro,__up__,to,"
		outString += str(self.n-1)+",do_macro("+str(self.n-1)+",__VA_ARGS__))\n"
		outString += "\n"
		self.s += outString
		return outString
	
	def __str__(self) :
		self.defineNumber()
		return self.s




def main(high,low,num_place_values) :
	for i in range(low,high+1):
		newNum = NumberDefinition(high+low-i,num_place_values,0)
		print newNum


main(16,3,1)
