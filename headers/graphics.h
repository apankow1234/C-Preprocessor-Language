/*
	Andrew Pankow
	Language c

	Linear Algebra (hopefully tensor calc) Library with Graphics Handles
*/


#ifndef GRAPHICS_H
#define GRAPHICS_H
#include "cnlppl.h"
//#include <math.h>


/*
	The DIVISION_PRECISION variable type will be what defines the precision of
	division by a scalar value and in obtaining a unit vector. Division only
	once so as to avoid unnecessary clock cycles. Default to double-precision.
*/
#ifndef DIVISION_PRECISION
	#define DIVISION_PRECISION double
#endif /* DIVISION_PRECISION */


// API
#define USE_VECTOR_API
#define USE_GRAPHICS
#ifdef USE_VECTOR_API

	#define mkvec(orderN,variableType) NVECTOR_CONSTRUCT(orderN,variableType,0)         // define a vector structure
	#define newVec(orderN,varName,...) NEW_VECTOR(orderN,0,varName,__VA_ARGS__)         // initialize a vector variable
	#ifdef USE_GRAPHICS
		#define mk2d(variableType) VECTOR2D_CONSTRUCT(variableType)                     // define 2D vectors for graphics
		#define mk3d(variableType)                                                      /* define 2-4D vectors for graphics*/ \
			mk2d(variableType); \
			VECTOR3D_CONSTRUCT(variableType); \
			VECTOR4D_CONSTRUCT(variableType)
		#define new2d(varName,...) NEW_VECTOR(2,1,varName,__VA_ARGS__)                  // initialize a 2D graphics vector variable
		#define new3d(varName,...) NEW_VECTOR(3,1,varName,__VA_ARGS__)                  // initialize a 3D graphics vector variable
		#define new4d(varName,...) NEW_VECTOR(4,1,varName,__VA_ARGS__)                  // initialize a 4D graphics vector variable
	#endif /* USE_GRAPHICS */

	#define addv(vec1,vec2,...)      VECTOR_ADDITION(vec1,vec2,0,__VA_ARGS__)           // vector plus vector
	#define adds(vec1,scalar,...)    VECTOR_ADDITION(vec1,scalar,1,__VA_ARGS__)         // vector plus scalar
	#define subv(vec1,vec2,...)      VECTOR_SUBTRACTION(vec1,vec2,0,__VA_ARGS__)        // vector minus vector
	#define subs(vec1,scalar,...)    VECTOR_SUBTRACTION(vec1,scalar,1,__VA_ARGS__)      // vector minus scalar
	#define multv(vec1,vec2,...)     VECTOR_MULTIPLICATION(vec1,vec2,0,__VA_ARGS__)     // vector multiplied by vector
	#define mults(vec1,scalar,...)   VECTOR_MULTIPLICATION(vec1,scalar,1,__VA_ARGS__)   // vector multiplied by scalar
	#define divv(vec1,vec2,...)      VECTOR_VECTOR_DIVISION(vec1,vec2,__VA_ARGS__)      // vector divided by vector
	#define divs(vec1,scalar,...)    VECTOR_SCALAR_DIVISION(vec1,scalar,__VA_ARGS__)    // vector divided by scalar
	#define inner(vec1,vec2,outval)  VECTOR_INNER_PRODUCT(vec1,vec2,outval)             // inner/dot product
	#define dot(vec1,vec2,outval)    VECTOR_INNER_PRODUCT(vec1,vec2,outval)             // dot/inner product
	#define len(vec1,outval)         VECTOR_LENGTH(vec1,outval)                         // vector length
	#define norm(vec1,lengthval,...) VECTOR_SCALAR_DIVISION(vec1,lengthval,__VA_ARGS__) // normalize a vector to a base
	#define unit(vec1,...)           VECTOR_UNIT(vec1,__VA_ARGS__)                      // normalize a vector to a base of 1
	#define cross3(vec1,vec2,outval) VECTOR_CROSS(3,vec1,vec2,outval)                   // 3Vector cross product
//	#define cross7(vec1,vec2,outval) VECTOR_CROSS(7,vec1,vec2,outval)                   // 7Vector cross product (coming soon?)
#endif /* USE_VECTOR_API */

/*
	Establishing the work environment (workspace/scope)...

	Creating vector structures to specifically and intentionally
	hold our vectors. There will be a little overhead regarding memory
	but going to at it's absolute minimum. Hoping to strike the best
	balance of readability with speed with size. It was important to me
	that the user of this library (mostly myself) not be limited to
	floats or doubles or ints... but to be able to choose and create
	custom types to be used as my vector elements. However, all elements
	in a vector must be of the same type since that's the point of
	defining the vector's type in the first place.

	Vectors will all have:
	* <<n>> An Order (number of elements/dimensions) that they maintain
	  for fewer cycles if/when we need it later (it's only an integer).

	* <<e>> Their Elements of whatever variable type you so desire.
	  I was aiming to produce as close to the <Template>T functionality
	  from c++ as I could because I really appreciate that fluidity but
	  wanted to avoid the overhead (miniscule as it may be).
	  I also wanted to make this scalable to be usable in mathematics and
	  physics so the baseline is general and not geared toward graphics.

	  * <<x|r>> If desired, variables x and r[ed] are assigned to point
	    the address of the first element in the vector.

	  * <<y|g>> If desired, variables y and g[reen] are assigned to point
	    the address of the second element in the vector.

	  * <<z|b>> If desired, variables z and b[lue] are assigned to point
	    the address of the third element in the vector.

	  * <<w|a>> If desired, variables w and a[lpha] are assigned to point
	    the address of the fourth element in the vector.
*/
#define VECTOR_DEFINITION(orderN,useGraphicsHandles) CLEAN(Vec##orderN,IF(useGraphicsHandles)(d,END()))
#define VECTOR_CONSTRUCTOR(orderN,variableType,useGraphicsHandles) \
	typedef struct VECTOR_DEFINITION(orderN,useGraphicsHandles) { \
		int n; variableType e[orderN]; \
		IF(AND(useGraphicsHandles,X_IN_BETWEEN(orderN,2,4))) \
			( \
				CLEAN(CONCAT(_,orderN),D_SETUP(variableType));\
				,END() \
			) \
	} VECTOR_DEFINITION(orderN,useGraphicsHandles)
// Dimensional Elements  _de[0-3]
#define _de0 x
#define _de1 y
#define _de2 z
#define _de3 w
// Color Elements  _ce[0-3]
#define _ce0 r
#define _ce1 g
#define _ce2 b
#define _ce3 a
/*
	Handles for graphics vectors.
	(x)
	(x,y)
	(x,y,z) and (r,g,b)
	(x,y,z,w) and (r,g,b,a)
*/
#define _1D_SETUP(variableType) variableType *_de0
#define _2D_SETUP(variableType) _1D_SETUP(variableType),*_de1
#define _3D_SETUP(variableType) _2D_SETUP(variableType),*_de2,*_ce0,*_ce1,*_ce2
#define _4D_SETUP(variableType) _3D_SETUP(variableType),*_de3,*_ce3

// General vector constructor... Could be nVector or Graphics Vector
#define NVECTOR_CONSTRUCT(orderN, variableType,...) \
		VECTOR_CONSTRUCTOR(orderN,variableType,NAND(EMPTY(__VA_ARGS__),CHECK(PROBE(__VA_ARGS__))))
// Predefined vectors able to be used in graphics.
#define VECTOR2D_CONSTRUCT(variableType) VECTOR_CONSTRUCTOR(2,variableType,1)
#define VECTOR3D_CONSTRUCT(variableType) VECTOR_CONSTRUCTOR(3,variableType,1)
#define VECTOR4D_CONSTRUCT(variableType) VECTOR_CONSTRUCTOR(4,variableType,1)

// Initializing new vectors.
#define INIT_ORDER(orderN) .n=orderN
#define INIT_0_ELEMENTS(orderN) .e={ 0 }
#define INIT_ELEMENTS(orderN,...) .e={ __VA_ARGS__ }
#define INIT_VECTOR(orderN,...) = {INIT_ORDER(orderN), \
	IF(AND(NOT(EMPTY(__VA_ARGS__)),NUM_ARGS_IS(orderN,__VA_ARGS__)))( \
		INIT_ELEMENTS(orderN,__VA_ARGS__) \
		,INIT_0_ELEMENTS(orderN) \
	)}
#define NEW_VECTOR(orderN,useGraphicsHandles,varName,...) \
	VECTOR_DEFINITION(orderN,useGraphicsHandles) varName \
	INIT_VECTOR(orderN,__VA_ARGS__)

// decrementing my index before use since arrays are zero based
#define VECTOR_COPY(in,out) (if(in.n == out.n){int i = in.n;do {out.e[--i] = in.e[i];} while(i);}})
// Slightly self-explanitory, but putting just one argument will set all elements to that value.
#define VECTOR_SET_ALL(out,...) out.e = { __VA_ARGS__ }


/*
	The meat and potatoes of the header... the math.

	Here, I start off by defining basic arithmetic on vectors using as few calls
	as possible and while desperately hanging onto the <Template>T functionality.
	Basically, all nVectors will be looped over (slight expense on cycles but
	necessary to maintain dynamicism). Will likely add macros specifically for
	2d-4d,7d, and 8d vectors to eliminate the 5 or 6 cycles it takes just to
	loop each time per element. I make it sound like a lot, it's not... but for
	some applications like astronomy or medical and especially path-tracing,
	it can add up.

	I have broken the macros up for the fullest dynamicism in my macros as well.
	I came up with this solution using JavaScript a while back and liked the elegance
	but hated the overhead that JavaScript comes with. Finding out I could do this
	in c was fantastic.
*/
#define COND_SELF(is_scalar,_1,_2) IF(NOT(is_scalar))(if(_1.n == _2.n),END())
#define COND_NORM(is_scalar,_1,_2,out) IF(NOT(is_scalar))(if(_1.n == _2.n && _1 == out.n),if(_1.n == out.n))
#define VEC_MATH_EXP(op,_1,cleaned_exp) {int i = _1.n;do{cleaned_exp;} while(i);}
#define EXP_SELF(is_scalar,op,_1,_2) _1.e[i] op##= IF(NOT(is_scalar))(_2.e[i],_2)
#define EXP_NORM(is_scalar,op,_1,_2,_3) _3.e[i] = _1.e[i] op IF(NOT(is_scalar))(_2.e[i],_2)
#define VECTOR_ARITHMETIC(op,is_scalar,in,other,...) \
	IF(EMPTY( __VA_ARGS__ )) \
	( \
		COND_SELF(is_scalar,in,other) \
			VEC_MATH_EXP(op,in,EXP_SELF(is_scalar,op,in,other)) \
		,COND_NORM(is_scalar,in,other,POP(__VA_ARGS__)) \
			VEC_MATH_EXP(op,in,EXP_NORM(is_scalar,op,in,other,POP(__VA_ARGS__))) \
	)
/*
	This is my work around for operator overloading in c and JavaScript.

	Since macros are just copy/paste strings, it was easy to select what operator
	would mean what and then pass it as a string through the macros... it gets
	compiled properly instead of trying to write functions using exec(); or
	whatever that procedure was called.
*/
#define VECTOR_ADDITION(_1,_2,_is_2_scalar,...) ({VECTOR_ARITHMETIC(+,_is_2_scalar,_1,_2,__VA_ARGS__)})
#define VECTOR_SUBTRACTION(_1,_2,_is_2_scalar,...) ({VECTOR_ARITHMETIC(-,_is_2_scalar,_1,_2,__VA_ARGS__)}) // vector minus vector
#define VECTOR_MULTIPLICATION(_1,_2,_is_2_scalar,...) ({VECTOR_ARITHMETIC(*,_is_2_scalar,_1,_2,__VA_ARGS__)}) // vector multiplied by vector
#define VECTOR_VECTOR_DIVISION(_1,_2,...) ({VECTOR_ARITHMETIC(/,0,_1,_2,__VA_ARGS__)}) // vector divided by vector
#define VECTOR_SCALAR_DIVISION(_1,_2,...) ({DIVISION_PRECISION val=1 / _2; VECTOR_ARITHMETIC(*,1,_1,_2,__VA_ARGS__)}) // vector divided by scalar
#define VECTOR_REV(vec) VECTOR_MULT_S(vec,-1) // Reverse the direction of the vector.
/*
	Vector methods that are not necessarily arithmetic.
*/
#define VECTOR_INNER_PRODUCT(vec1,vec2,out) ({if(vec1.n==vec2.n){out=0;int i=vec1.n;do {out+=vec1.e[--i] * vec2.e[i];} while(i);}})
#define VECTOR_LENGTH(vec,out) sqrt(VECTOR_INNER_PRODUCT(vec,vec,out))
#define VECTOR_NORMALIZE(vec,length,...) VECTOR_SCALAR_DIVISION(vec,length,__VA_ARGS__)
#define VECTOR_UNIT(vec,...) DIVISION_PRECISION length=VECTOR_LENGTH(vec,length);VECTOR_NORMALIZE(vec,length,__VA_ARGS__))
#define VECTOR_CROSS(orderN,vec1,vec2,out) \
	({ \
		out.e[0]=(vec1.e[1] * vec2.e[2] - vec1.e[2] * vec2.e[1]); \
		out.e[1]=-(vec1.e[0] * vec2.e[2] - vec1.e[2] * vec2.e[0]); \
		out.e[2]=(vec1.e[0] * vec2.e[1] - vec1.e[1] * vec2.e[0]); \
	})

#define RAY_POINT_AT(ray,percent,...)


#endif /* GRAPHICS_H */
