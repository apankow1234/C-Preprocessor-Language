/*
	Andrew Pankow
	Code for 30 days
	Language c : Day 2

	Following along with Raytracing in One Weekend series - Shirley,
	Physically Based Rendering - Pharr, and other online sources. I'm adapting
	all code to be my own for my own learning purposes and not
	necessarily the best rendering practices.
*/

#include <stdio.h>
#include <time.h>
#define USE_VECTOR_API
#define USE_GRAPHICS
#include "headers/graphics.h"

mkworld(double);


//int hit_sphere(const Vector3d& center, double radius, const Ray3d& r)
//{
//	Vector3d oc = r.o;
//	VECTOR_SUB_S(oc,center);
//	double a=dot3(r.d,r.d);
//	double b=2.0*dot3(oc,r.d);
//	double c=dot3(oc,oc)-radius*radius;
//	double discriminant=b*b-(4*a*c);
//	return (discriminant>0?1:0);
//}


/*
	actual rendering function
*/
Vector3d color(const Ray3d &ray)
{
	/*Collision Colors*/
	Vector3d hit_color;
	newVec3d(sphere_center,0.0,0.0,-1);
	double t = hit_sphere(sphere_center,0.5,ray);
	if(t>0.0)
	{
		Vector3d N,n;
		newVec3d(away,0.0,0.0,1.0);
		point3At(ray,t,n);
		subs3(n,away);
		units3(n);
		setVec3(N,n)
		csetVec3(hit_color,1.0,0.0,0.0);
	}
	return hit_color;

	/*World Background Color*/
	Vector3d unit_dir,bg_color;
	newVec3d(unit_base,1.0,1.0,1.0);
	newVec3d(color,0.5,0.7,1.0); /*sky blue*/

	unit3(ray.d,unit_dir); /*Get normalized ray direction for direction*/
	double t=0.5f*(unit_dir.y+1.0);
	smults3(color,t); /*multiply color by t back to color*/
	smults3(unit_base,(1.0-t)); /*multiply color by the reciprocal of t back to color*/
	add3(color,unit_base,bg_color);

	return bg_color; /*somewhere between sky blue and white*/
}

int main()
{
	/*timer setup*/
	time_t begin, finish;
	time(&begin);

	/*output setup*/
	int pixel_width = 1920;
	int pixel_height = 1280;
	char *filepath="";
	FILE *fptr;
	fptr = fopen("/home/drew/workspace/graphics-one_per_day/renders/test_hds.ppm","w+");
	fprintf(fptr,"P3\n%d %d \n255\n",pixel_width,pixel_height);

	/*camera setup*/
	Vector3d offset,offset2,col;
	newVec3d(lower_left_corner,-2.0,-1.0,-1.0);
	newVec3d(horizontal,4.0,0.0,0.0);
	newVec3d(vertical,0.0,2.0,0.0);
	newVec3d(origin,0.0,0.0,0.0);

	/*render process*/
	Ray3d ray;
	for(int j=pixel_height-1;j>=0;j--)
	{
		for(int i=0;i<pixel_width;i++)
		{
			ray.o=origin; /*start from the back of the camera*/

			/*determine direction of ray*/
			double u=(double)i/(double)pixel_width;
			double v=(double)j/(double)pixel_height;
//			mults(horizontal,u,offset);
//			mults(vertical,v,offset2);
			addv(offset,offset2);
//			addv(lower_left_corner,offset,ray.d); /*aim through a pixel*/

			/*obtain color*/
			setVec3(col,color(ray));
			int r = (int)(255.99*(col.x));
			int g = (int)(255.99*(col.y));
			int b = (int)(255.99*(col.z));

			/*render scene*/
			fprintf(fptr,"%d %d %d\n",r,g,b);
		}
	}
	/*timer end*/
	time(&finish);
	double seconds = difftime(begin, finish);
	fprintf(fptr,"\n\n\nRendered in: %d seconds\n",seconds);
	fclose(fptr);
	
	/*End Scene*/
	return 0;
};
