from QA_Generator import QA_Generator


class QA_PV_Equality(QA_Generator):
    
    def __init__(self):
        self.test_lines = []
        self.    ops = {
            "eq": {
                    "name":"equality"
                    ,"macro":"nEq"
                    ,"op":"=="
                }
            ,"ne" : {
                    "name":"inequality"
                    ,"macro":"nNEq"
                    ,"op":"!="
                }
            ,"lt" : {
                    "name":"less than"
                    ,"macro":"nLT"
                    ,"op":"<"
                }
            ,"le" : {
                    "name":"less than or equal to"
                    ,"macro":"nLE"
                    ,"op":"<="
                }
            ,"gt" : {
                    "name":"greater than"
                    ,"macro":"nGT"
                    ,"op":">"
                }
            ,"ge" : {
                    "name":"greater than or equal to"
                    ,"macro":"nGE"
                    ,"op":">="
                }
        }