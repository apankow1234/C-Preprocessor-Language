import os,re



def place_value_eq():
    ops = {
        "eq": {
                "name":"equality"
                ,"macro":"nEq"
                ,"op":"=="
            }
        ,"ne" : {
                "name":"inequality"
                ,"macro":"nNEq"
                ,"op":"!="
            }
        ,"lt" : {
                "name":"less than"
                ,"macro":"nLT"
                ,"op":"<"
            }
        ,"le" : {
                "name":"less than or equal to"
                ,"macro":"nLE"
                ,"op":"<="
            }
        ,"gt" : {
                "name":"greater than"
                ,"macro":"nGT"
                ,"op":">"
            }
        ,"ge" : {
                "name":"greater than or equal to"
                ,"macro":"nGE"
                ,"op":">="
            }
    }
    
    
    tests = []
    for operator in ops:
        #print ops[operator]["name"]
        #print ops[operator]["macro"]
        #print ops[operator]["op"]
        print "printf(\""+ops[operator]["name"].upper()+" >> \\n\");"
        op_test = []
        for num1_pv1 in range(10):
            for num1_pv2 in range(2):
                for num2_pv1 in range(10):
                    for num2_pv2 in range(2):
                        num1_pv1 = str(num1_pv1)
                        num1_pv2 = str(num1_pv2)
                        num2_pv1 = str(num2_pv1)
                        num2_pv2 = str(num2_pv2)
                        
                        macro = "," + ops[operator]["macro"] + "("
                        macro += "CSN("+",".join([num1_pv1,num1_pv2])+")"
                        macro += ","
                        macro += "CSN("+",".join([num2_pv1,num2_pv2])+"))"
                        
                        outstr = "printf(\"%d%d "+ops[operator]["op"]+" %d%d"+macro+",%d\\n\","
                        outstr += ",".join([num1_pv2,num1_pv1,num2_pv2,num2_pv1])
                        outstr += macro
                        outstr += "==("+str(int(num1_pv2)*10+int(num1_pv1))
                        outstr += " "+ops[operator]["op"]+" "
                        outstr += str(int(num2_pv2)*10+int(num2_pv1))+")"
                        outstr += ");"
                        
                        op_test.append( outstr )
        tests.append(op_test)
    print tests
place_value_eq()