/*
	Andrew Pankow
	Language c
*/
#define USE_VECTOR_API
#define USE_GRAPHICS
#include "../headers/graphics.h"

//mk3d(double);
//mkvec(3, float);
//mkvec(8, int);
//mkvec(4, double);


//#define DO_FOR(do_macro,breakout_macro,from,to,prev_return,...) \
//	IF(NOT_EQUAL(from,to)) \
//	( \
//		CLEAN(PROTO_FROM_,from) \
//		,END \
//	)(do_macro,breakout_macro,X_GREATER_THAN(to,from),to,0,prev_return,__VA_ARGS__)

//#define PROTO_DO_FOR(do_macro,breakout_macro,from,to,prev_return,...) \
//	IF(NOT_EQUAL(from,to)) \
//	( \
//		CLEAN(PROTO_FROM_,from) \
//		,END \
//	)(do_macro,breakout_macro,X_GREATER_THAN(to,from),to,0,prev_return,__VA_ARGS__)
//#define CLEAN_LIST(...) IF(IS_FIRST_EMPTY(__VA_ARGS__))(DUMP_FIRST_ARG,RUN)(__VA_ARGS__)


int main(int *argc,char **argv)
{
//	new3d(v1);
//	new2d(v2,52.2021526,2);
//	new3d(v3);
//	v1.e[0]=30.0f;

//	UP(UP(UP(UP(UP(UP(UP(UP(UP(CSN(0))))))))))
//		;
//	UP(UP(UP(UP(UP(UP(UP(UP(UP(UP(CSN(0)))))))))))
//	;
//	UP(UP(CSN(0)))
//		;

	_pv_10s();
	CSN(0);
	CSN(3);
	CSN(9);

	pvGE_10s(CSN(0,2),CSN(9,2));

	_cmp_10s(CSN(9,2,4,5));
	_cmp_10s(CSN(9,2,4,5));
	_cmp_1s(CSN(9,2,4,5));
	_primary_num(CSN(9,2,4,5),CSN(9,2,4,5));
	_secondary_num(CSN(9,2,4,5),CSN(9,2,4,5));

	nEq(CSN(9,2,4,5),CSN(9,2,4,5));
	nEq(CSN(9,2,4,5),UP(CSN(9,2,4,5)));
	nNEq(CSN(9,2,4,5),CSN(9,2,4,5));
	nNEq(CSN(9,2,4,5),UP(CSN(9,2,4,5)));

//	_pv_1s();
//	_pv_10s();
//	_pv_100s();
//	_pv_1000s();

//	_range(CSN(0),CSN(5))();
//	WHEN(0)(PROTO_DO_FOR(CLEAN_SEQ,DISP_SEQ_TO,0,8,));
//
//	PROTO_DO_FOR(CLEAN_SEQ,DISP_SEQ_TO,0,1,)
//	;
//	PROTO_DO_FOR(CLEAN_SEQ,DISP_SEQ_TO,1,0,)
//	;
//	PROTO_DO_FOR(CLEAN_SEQ,DISP_SEQ_TO,0,2,)
//	;
//	PROTO_DO_FOR(CLEAN_SEQ,DISP_SEQ_TO,2,0,)
//	;
//	PROTO_DO_FOR(CLEAN_SEQ,DISP_SEQ_TO,0,9,)
//	;
//	PROTO_DO_FOR(CLEAN_SEQ,DISP_SEQ_TO,9,0,)
//	;
//	PROTO_DO_FOR(CLEAN_SEQ,DISP_SEQ_TO,9,0,DO_FOR(CLEAN_SEQ,DISP_SEQ_TO,9,0,))
//	;
//	RASTERIZE_NUMBER(PROTO_DO_FOR(dumb_counter,disp_count,9,3,))
//	;
//	RASTERIZE_NUMBER(PROTO_DO_FOR(dumb_counter,disp_count,1,5,))
//	;
//
//	RASTERIZE_NUMBER(DO_FOR(dumb_counter,disp_count,0,9,))
//	;

	nfrom_0(qRas,qRas,CSN(0),CSN(4),CSN(0));
//	nfrom_0(RUN,END,CSN(0),CSN(9),CSN(0));

//
//	_range_1s(CSN(9))
//	;

//	Vec2d testVec;
//
//	newVec(8,octavec,15,64,1,2,8,4126,564,98);
//
//	divs(v1,5,v3);
//	adds(v1,5);
//	addv(v1,v2);

//	int x,y,z;
//	x=2;
//	y=3;

	return 0;
}

