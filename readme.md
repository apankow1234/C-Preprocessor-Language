# C's Preprocessor Language

## Under Construction!

See [Status Doc](status.md)

## Building Macros into Proto-Functions

This started out as a 30-day challenge to myself to simply code. Initially heading toward the achievement of a standard program for the purposes of portfolio work for getting hired.

My goal has shifted from a self-challenge to an actual project. I got sick of the grey-area between great c and great c++. So many programmers that I have met are willing to give up so much hardware (or aren't even aware) for the ability to have easy to read code and I can completely understand the argument; however, I felt there was another way. 

### Workarounds in the preprocessor to enable spreadsheet-like functionality to which most are accustomed

By putting the overhead on the preprocessor's Copy/Paste simplicity, we can build up vicous little macros that count the number of arguments passed, construct arithmetic, and perform the code encapsulation (visually) that make the applications easier to parse. 

For this, I was willing to accept higher compile times to acheive these beautified outputs for very low run-times. However, it seems that compile time is miniscule on modern machines, including the Raspberry-Pis. 

### Documentation

* [AND(A,B)](docs/AND.md)
* ARGUMENT_OFFSET(_1,_2,,,_99,N,...)
* BOOL(X)
* [__check(X)](docs/__check.md)
* [__check_n(X,N...)](docs/__check_n.md)
* [CLEAN](docs/CLEAN.md)
* [__compliment(X)](docs/__compliment.md)
* [CONCAT](docs/CONCAT.md)
* END(...)
* EQUAL(A,B)
* IF(x)(trueValue,falseValue)
* [__iif(X)(trueValue,falseValue)](docs/__iif.md)
  * [__iif_1(trueValue,falseValue)](docs/__iif_1.md)
  * [__iif_0(trueValue,falseValue)](docs/__iif_0.md)
* INVERSE_SEQUENCE()
* IS_BLANK(...)
* IS_DEFINED(prefix,X)
* IS_DEFINED_NUMERAL(prefix,X)
* [IS_PAREN(x)](docs/IS_PAREN.md)
  * [__is_paren_probe(...)](docs/__is_paren_probe.md)
* NAND(A,B)
* NOR(A,B)
* [NOT(X)](docs/NOT.md)
  * [__not_0](docs/__not_0.md)
* NOT_EQUAL(A,B)
* [OR(A,B)](docs/OR.md)
* [__probe(X)](docs/__probe.md)
* PROTO_COUNT(...)
  * POP_COUNT(...)
* RUN(...)
* WHEN(X)(trueValue)
* X_GREATER_THAN(X,MIN)
* X_GREATER_THAN_OR_EQUAL_TO(X,MIN)
* X_LESS_THAN(X,MAX)
* X_LESS_THAN_OR_EQUAL_TO(X,MAX)
* [XOR(A,B)](docs/XOR.md)