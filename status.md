# Status

## Current 

Macro Dependency Chart
![Macro Dependencies](docs/images/macro_dependencies1.svg)

## To Do List

Still working on :
- [ ] QA Tests
- [ ] Multiple Place-value Comparison
- [ ] Function passing loops
- [ ] Dynamic argument counting
- [ ] Documentation
- [ ] Build API of Functionality

Completed :
- [x] Basic Looping
- [x] Conditional branching
- [x] Numbers (based 2 to 99)
- [x] Binary Comparison
	* Or
	* Nor
	* And
	* Nand
	* Xor
- [x] Single Place-value Comparison (bases 2 to 99)
	* Equality/Inequality
	* Less Than
	* Less Than or Equal To
	* Greater Than
	* Greater Than or Equal To
- [x] Ability to count arguments with non-dynamic pop-method
- [x] Ability to check for blanks and parentheses
